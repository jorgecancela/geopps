package com.geopps.android;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.geopps.android.db.AlertsDataSource;
import com.geopps.android.utils.Alert;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.DataParcelable;
import com.geopps.android.utils.Place;
import com.geopps.android.utils.SelectAreaItemizedOverlay;
import com.geopps.android.utils.Tools;

/**
 * Activity to show an Alert. It will display a map with the alert position
 * and the user position in real time. Then all the details of the alerts: ringtone, date, time
 * and comment.
 * 
 * The title bar will display a edit button to go AlertsActivitySave activity to edit the alert
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class AlertActivityShow extends Activity {
    private AlertsDataSource aDatasource;
    private Drawable uMarker, aMarker;
    private TextView tvName, tvNote;
    private Place currentPlace;
    private Button editButton, ringtoneButton, startDateButton, endDateButton, timezoneButton, deleteAlert;
    private Alert currentAlert;
    private MapView mapView;
    private RealTimeUserTracker myRealTimeUserTracker;
    private String LOGGER_TAG = "AlertActivityShow";
    private Boolean firstTime = true;
    private AlertDialog alertDialog;

    public SelectAreaItemizedOverlay myItemizedOverlay = null;
    public SelectAreaItemizedOverlay myUserOverlay = null;
    public List<GeoPoint> alertToShow;

    @Override
    protected void onPause() {
	try {

	    aDatasource.close();
	    onRealTimeUserTrackerPause();

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception onPause: " + e.toString());
	}
	super.onPause();
    }

    @Override
    protected void onResume() {
	try {

	    onRealTimeUserTrackerResume();

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception onResume: " + e.toString());
	}
	super.onResume();
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	// Read the last configuration stored in the database and setup the configuration
	try {

	    Tools.setSettings(this);
	    Tools.setContext(this.getApplicationContext());

	    alertDialog =  new AlertDialog.Builder(AlertActivityShow.this).create();

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception setting up context: " + e.toString());
	}

	setContentView(R.layout.alert_show);

	mapView = (MapView) findViewById(R.id.mapViewAlertsShow);
	mapView.setBuiltInZoomControls(true);
	mapView.setClickable(false);

	tvName = (TextView) findViewById(R.id.textViewAlertShowTitle);
	tvNote = (TextView) findViewById(R.id.textViewAlertShowComment);

	startDateButton = (Button) findViewById(R.id.buttonStartDay);
	endDateButton = (Button) findViewById(R.id.buttonEndDay);
	timezoneButton = (Button) findViewById(R.id.buttonTimeZone);
	ringtoneButton = (Button) findViewById(R.id.buttonRingtoneAdd);

	try {

	    // Open the database and read the intent with the Alert ID.
	    aDatasource = new AlertsDataSource(this.getApplicationContext());

	    Intent i = getIntent();
	    DataParcelable currentAlertID = (DataParcelable) i.getParcelableExtra(Constants.ALERT_ID_PARCEL_STRING);

	    // Get the alert details and show them
	    aDatasource.open();
	    currentAlert = aDatasource.getAlertById(currentAlertID.intParcelable);

	    aDatasource.close();

	    currentPlace = currentAlert.getPlace();
	    tvName.setText(currentPlace.getName());
	    tvNote.setText(currentAlert.getComment());
	    timezoneButton.setText(currentAlert.getTimezone());

	    if(currentAlert.getEndDate().get(Calendar.YEAR) == Constants.MAX_CALENDAR_YEAR){
		endDateButton.setText(Tools.getStringResource(R.string.alerts_add_end_day_title)+ " " +Tools.getStringResource(R.string.alerts_add_end_day_content));		
	    }else{
		endDateButton.setText(Tools.getStringResource(R.string.alerts_add_end_day_title)+ " " +Tools.CalendarDateToStringToShow(currentAlert.getEndDate()));
	    }

	    if(Tools.isToday(currentAlert.getStartDate())){
		startDateButton.setText(Tools.getStringResource(R.string.alerts_add_start_day_title)+ " " +Tools.getStringResource(R.string.alerts_add_start_day_content));
	    }else{
		startDateButton.setText(Tools.getStringResource(R.string.alerts_add_start_day_title)+ " " +Tools.CalendarDateToStringToShow(currentAlert.getStartDate()));
	    }

	    Uri uri;
	    
	    if ((currentAlert.getRingtone() == null)||(currentAlert.getRingtone().compareTo("")==0)){	
		uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
	    }else{
		uri = Uri.parse(currentAlert.getRingtone());
	    }

	    Ringtone ringtone = RingtoneManager.getRingtone(this.getApplicationContext(), uri); 
	    ringtoneButton.setText(ringtone.getTitle(this.getApplicationContext()));

	    
	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception OnCreate phase: " + e.toString());
	}

	editButton = (Button) findViewById(R.id.buttonAlertShowEdit);
	editButton.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {

		    // Edit button heads to AlertsActivitySave Activity adding the alert ID as input parameter
		    DataParcelable dparcelable = new DataParcelable((int) currentAlert.getId());  

		    Intent k = new Intent(AlertActivityShow.this, AlertsActivitySave.class);
		    k.putExtra(Constants.ALERT_ID_PARCEL_STRING, dparcelable);
		    startActivity(k);

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception starting AlertsActivitySave: " + e.toString());
		}
	    }
	});

	deleteAlert = (Button) findViewById(R.id.buttonAlertShowDiscard);
	deleteAlert.setOnClickListener(new View.OnClickListener() { 

	    @Override
	    public void onClick(View view) {
		try {	

		    //Ask the user if they want to delete the patient
		    //AlertDialog alertDialog =  new AlertDialog.Builder(myTools.getContext()).create();
		    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
		    alertDialog.setTitle(R.string.alerts_activity_discard_title);
		    alertDialog.setMessage(Tools.getStringResource(R.string.alerts_activity_discard_message));
		    alertDialog.setButton(Tools.getStringResource(R.string.alerts_activity_discard_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {			    
			    aDatasource.open();
			    aDatasource.deleteAlert((long) currentAlert.getId());

			    Intent k = new Intent(AlertActivityShow.this, AlertsActivity.class);
			    startActivity(k); 

			    aDatasource.close();

			    Tools.showToastMessage(Tools.getContext(), Tools.getStringResource(R.string.alerts_activity_disard_confirmation_message));
			}
		    });

		    alertDialog.setButton2(Tools.getStringResource(R.string.alerts_activity_discard_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		    });

		    alertDialog.show();


		} catch(Exception e) {
		    Log.v(LOGGER_TAG, "Error deleting alert: " + e.toString());
		}	        		
	    }
	}); 

	try {

	    // Plot the user marker and the alert place marker
	    aMarker = getResources().getDrawable(R.drawable.place);

	    uMarker = getResources().getDrawable(R.drawable.marker2);

	    ResourceProxy resourceProxy = new DefaultResourceProxyImpl(this.getApplicationContext());

	    myUserOverlay = new SelectAreaItemizedOverlay(uMarker, resourceProxy);
	    myUserOverlay.setBottomCenter(true);
	    myUserOverlay.setTouchable(false);

	    myItemizedOverlay = new SelectAreaItemizedOverlay(aMarker, resourceProxy);
	    myItemizedOverlay.setTouchable(false);

	    mapView.getOverlays().add(myUserOverlay);
	    mapView.getOverlays().add(myItemizedOverlay);

	    GeoPoint currentPoint = new GeoPoint(currentPlace.getLat() / 1E6, currentPlace.getLon() / 1E6);

	    // For the place alert it plots the marker AND the area of the alert
	    myItemizedOverlay.setArea(currentPoint, currentPlace.getRadius());
	    myItemizedOverlay.addItem(currentPoint, currentPlace.getName(), currentPlace.getName());

	    alertToShow = new ArrayList<GeoPoint>();
	    alertToShow.add(currentPoint);

	    if(Tools.lastGeoPoint != null){

		//alertToShow.add(Tools.lastGeoPoint);
		myUserOverlay.clear();
		myUserOverlay.addItem(Tools.lastGeoPoint, "","");

		ArrayList<GeoPoint> geoPointsToShow = new ArrayList<GeoPoint>();	    
		geoPointsToShow.addAll(alertToShow);

		BoundingBoxE6 bbE = BoundingBoxE6.fromGeoPoints(geoPointsToShow); 

		mapView.getController().setCenter(bbE.getCenter());
		mapView.zoomToBoundingBox(bbE);

	    }else{

		mapView.getController().setCenter(currentPoint);
		mapView.getController().setZoom(Tools.calculateZoomLevel(currentPlace.getRadius()));	

	    }

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception populating the map: " + e.toString());
	}
    }

    private void onRealTimeUserTrackerPause() {
	myRealTimeUserTracker.stop();
	myRealTimeUserTracker = null;
    }

    private void onRealTimeUserTrackerResume() {
	myRealTimeUserTracker = new RealTimeUserTracker();
	myRealTimeUserTracker.execute();
    }

    // This thread updates the user position
    //
    //
    private class RealTimeUserTracker extends AsyncTask<Void, String, Void> {

	private boolean running = true;

	@Override
	protected Void doInBackground(Void... params) {
	    while( running ) {

		try {

		    publishProgress();
		    Thread.sleep(20000);

		} catch (Exception e) {
		    Log.v(LOGGER_TAG, "Exception e: "  + e.toString());
		} 
	    }

	    return null;
	}

	@Override
	protected void onProgressUpdate(String... values) {


	    BoundingBoxE6 bbE = Tools.updateMapView(myUserOverlay, mapView, alertToShow, true, firstTime);	
	    firstTime = false;

	    mapView.getController().setCenter(bbE.getCenter());

	    if(firstTime){

		mapView.zoomToBoundingBox(bbE);

	    }else{
		mapView.zoomToBoundingBox(bbE);
		int zoomLevel = mapView.getZoomLevel();

		// TODO (jcancela) esto es un fallo en OSM
		if (zoomLevel > 3)
		    mapView.getController().setZoom(zoomLevel-1);
	    }

	    int zoomC = Tools.calculateZoomLevel(currentPlace.getRadius());

	    if (zoomC < mapView.getZoomLevel()){
		mapView.getController().setZoom(zoomC);
	    }

	    mapView.invalidate();

	    if( ! running ) {
		return; 
	    }
	}

	public void stop() {
	    running = false;
	}
    }
}
