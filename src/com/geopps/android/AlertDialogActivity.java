package com.geopps.android;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;

import com.geopps.android.db.AlertsDataSource;
import com.geopps.android.utils.Alert;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.DataParcelable;
import com.geopps.android.utils.Tools;

/**
 * This activity shows an alert message to the user both if the user is in
 * Geopps or not. An alertDialog will be display anywhere, to do that, it is
 * needed to use a special Acitvity design, in the Manifest we need to specify a
 * translucent theme, and add a launchMode and noHistory flags
 * 
 * <activity android:name="com.geopps.android.AlertDialogActivity"
 *    android:launchMode="singleInstance" 
 *    android:noHistory="true"
 *    android:theme="@android:style/Theme.Translucent.NoTitleBar.Fullscreen" >
 * </activity>
 * 
 * Doing this, the activity will be transparent and we will be able to launch
 * an alertDialog. If the user only wants to deactivate the alerts we need to call
 * super.onBackPressed(); to return to the previous Activity.
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class AlertDialogActivity extends Activity {
    public static AlertDialog alertDialog;
    private Alert a;
    private static String LOGGER_TAG = "AlertDialogActivity";
    
    private static Boolean isRinging = false;
    public static NotificationManager mManager;
    public static final int APP_ID_NOTIFICATION = 999;

    public StringBuilder stringMessage = new StringBuilder();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	// To avoid multiple call every Tools.timerUpdateMilliseconds
	if (!isRinging){
	    
	    isRinging = true;

	    // Notifications
	    mManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	    Notificar();
	    
	    // Alert system
	    alertDialog = new AlertDialog.Builder(this).create();
	    showAlert();
	}
    }

    public void returnAfterShow() {
	super.onBackPressed();
    }
    
    /**
     * Function to show settings alert dialog
     * */
    private void showAlert() {
	// Alert message
	alertDialog.setIcon(R.drawable.ic_launcher);

	AlertsDataSource datasource = new AlertsDataSource(Tools.getContext());
	datasource.open();

	alertDialog.setTitle(Tools.alertsInRange.size() + " Geopps alerts");

	for (int aid : Tools.alertsInRange) {

	    a  = datasource.getAlertById(aid);

	    stringMessage.append(a.getPlace().getName());
	    stringMessage.append(", ");
	    stringMessage.append(a.toStringCommentLimited(Constants.TEXT_LIMITED_20_CHARS));
	    stringMessage.append("\n");

	}

	datasource.close();

	try{
    
	    Uri uri;
	    
	    if (a.getRingtone() == null){	
		uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
	    }else{
		uri = Uri.parse(a.getRingtone());
	    }
	    
	    Tools.playSound(this, uri);

	    Vibrator vi = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	    vi.vibrate(500);

	}catch(Exception e){

	    Log.v(LOGGER_TAG,"Exception: " + e.toString());
	    
	}

	alertDialog.setMessage(stringMessage);
	alertDialog.setCancelable(false);
	alertDialog.setButton(Tools.getStringResource(R.string.alert_dialog_swith_off), new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		
		if (isRinging()){

		    setIsRinging(false);
		stopAlerts();
		
		mManager.cancel(APP_ID_NOTIFICATION);		
		
		returnAfterShow();
		}
	    }
	});

	alertDialog.setButton2(Tools.getStringResource(R.string.alert_dialog_go), new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		// Go back to edition form
		Tools.alertsRaised = (ArrayList<Alert>) stopAlerts();
		mManager.cancel(APP_ID_NOTIFICATION);		

		try {

		    DataParcelable dparcelable = new DataParcelable(Constants.SHOW_RAISED_ALERTS);  
	    
		    Intent k = new Intent(AlertDialogActivity.this, MapShowActivity.class);
		    k.putExtra(Constants.MAP_TYPE_ID_PARCEL_STRING, dparcelable);
		    startActivity(k);

		} catch (Exception e) {
		    Log.v("AlertsActivitySave", "Error: " + e.toString());
		}
	    }
	});
	
	alertDialog.show();
    }
    
    
    /**
     * prepara y lanza la notificacion
     */
    private void Notificar() {
		
	//Prepara la actividad que se abrira cuando el usuario pulse la notificacion
	Intent intentNot = new Intent(this, MapShowActivity.class);   
	DataParcelable dparcelable = new DataParcelable(Constants.SHOW_RAISED_ALERTS);  
	intentNot.putExtra(Constants.MAP_TYPE_ID_PARCEL_STRING, dparcelable);
	    
	//Prepara la notificacion
	Notification notification = new Notification(R.drawable.ic_launcher, "Geopps alerts", System.currentTimeMillis());
	notification.setLatestEventInfo(this, getString(R.string.app_name), stringMessage,
	PendingIntent.getActivity(this.getApplicationContext(), 0, intentNot, PendingIntent.FLAG_CANCEL_CURRENT));
	
	//Le a�ade sonido
	notification.defaults |= Notification.DEFAULT_ALL;
	notification.flags = Notification.FLAG_AUTO_CANCEL;

	try
	{
	    notification.ledARGB = 0xff00ff00;
	    notification.ledOnMS = 300;
	    notification.ledOffMS = 1000;
	    notification.flags |= Notification.FLAG_SHOW_LIGHTS;
	    
	}catch(Exception e){
	    
	    Log.e(LOGGER_TAG, e.toString());
	
	}

	try{
	
	    mManager.notify(APP_ID_NOTIFICATION, notification);
	
	}catch(Exception e){
	    Log.e(LOGGER_TAG, e.toString());
	}	
    }
    
    public static Boolean isRinging(){
	return isRinging;
    }
    
    public static Boolean setIsRinging(Boolean b){
	return isRinging = b;
    }
    
    public static List<Alert> stopAlerts(){
	
	Tools.mMediaPlayer.stop();
	setIsRinging(false);
	
	try{
		
	    alertDialog.cancel();
	
	}catch(Exception e){
	    Log.e(LOGGER_TAG, e.toString());
	}
	
	AlertsDataSource datasource = new AlertsDataSource(Tools.getContext());
	datasource.open();

	List<Alert> alertsRaised = new ArrayList<Alert>();

	for (int aid : Tools.alertsInRange) {

	    Alert a = datasource.getAlertById(aid);
	    a.setAlarmActive(false);
	    alertsRaised.add(datasource.getAlertById(aid));

	}

	datasource.close();
	
	return alertsRaised;
    }
}
