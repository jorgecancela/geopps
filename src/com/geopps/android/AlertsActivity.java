package com.geopps.android;

import java.util.List;
import org.osmdroid.util.GeoPoint;

import android.app.AlertDialog;
import android.app.ListActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.geopps.android.db.AlertsDataSource;
import com.geopps.android.db.PlacesDataSource;
import com.geopps.android.utils.Alert;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.DataParcelable;
import com.geopps.android.utils.Place;
import com.geopps.android.utils.Tools;

/**
 * This is the main Activity. It shows the list of all the alerts. 
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class AlertsActivity extends ListActivity {
    private Button addAlertButton, settingsButton, mapButton, refreshButton;
    private AlertsDataSource datasource;
    private PlacesDataSource place_datasource;
    private AlertDialog alertDialog;
    private ArrayAdapter<Alert> adapter;
    private List<Alert> values;
    private ListView lv;
    private String LOGGER_TAG = "AlertsActivity";
    private static Boolean booting = true;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	// Read the last configuration stored in the database and setup the configuration	
	try {

	    Tools.setSettings(this.getApplicationContext());
	    Tools.setContext(this.getApplicationContext());

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception setting up context: " + e.toString());
	}

	if (booting){
	    Tools.init();
	    booting = false;
	}

	setContentView(R.layout.alerts_main);

	// Check if GPS service is available. If not show an alert message to activate it
	alertDialog = new AlertDialog.Builder(this).create(); 

	final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

	if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
	    showGPSAlert();
	}

	addAlertButton = (Button) findViewById(R.id.buttonAddNew);
	addAlertButton.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {

		    datasource.close();
		    Intent k = new Intent(AlertsActivity.this, PlacesActivity.class);
		    startActivity(k);

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception starting PlacesActivity: " + e.toString());
		}
	    }
	});

	settingsButton = (Button) findViewById(R.id.buttonAlertsMainSettings);
	settingsButton.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View view) {
		try {

		    datasource.close();
		    Intent k = new Intent(AlertsActivity.this, SettingsActivity.class);
		    //Intent k = new Intent(AlertsActivity.this, GoogleCalendarActivity.class);
		    startActivity(k);

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception starting MapShowActivity: " + e.toString());
		}
	    }
	});

	refreshButton = (Button) findViewById(R.id.buttonAlertsMainRefresh);
	refreshButton.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {

		    // Check in Google Calendar for new events

		    // if new events add in local

		    // Do all the places have a coincidence in local?? 

		    // Yes  -> Add
		    // No -> Notify
		    //Notificar();

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception starting Refresh: " + e.toString());
		}
	    }
	});
	
	if ((Build.VERSION.SDK_INT < 11)||(!Constants.GCALENDAR_SYNC_FLAG)){
	    refreshButton.setVisibility(Button.GONE);
	}

	mapButton = (Button) findViewById(R.id.buttonAlarmsMainMap);
	mapButton.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View view) {
		try {

		    datasource.close();

		    DataParcelable dparcelable = new DataParcelable(Constants.SHOW_ACTIVE_ALERTS);  

		    Intent k = new Intent(AlertsActivity.this, MapShowActivity.class);
		    k.putExtra(Constants.MAP_TYPE_ID_PARCEL_STRING, dparcelable);
		    startActivity(k);

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception starting MapShowActivity: " + e.toString());
		}
	    }
	});

	try {

	    datasource = new AlertsDataSource(this.getApplicationContext());
	    place_datasource = new PlacesDataSource(this.getApplicationContext());

	    datasource.open();
	    place_datasource.open();

	    values = datasource.getAllAlerts();

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception opening database: " + e.toString());
	}

	adapter = new AlertAdapter(this, R.layout.alerts_main_item_list, values);
	setListAdapter(adapter);

	lv = getListView();
	lv.setTextFilterEnabled(true);

	// Warning! : Inside this lv we have a ToggleButton which also has an onClickListener, it makes that this lv onClickListener
	// doesn't work unless we add this line  android:focusable="false" in the ToggleButton xml file
	lv.setOnItemClickListener(new OnItemClickListener() {
	    @Override
	    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		try {

		    Alert alert = (Alert) lv.getAdapter().getItem(position);

		    try {	
			
			DataParcelable dparcelable = new DataParcelable((int) alert.getId());  
			datasource.close();

			Intent k = new Intent(AlertsActivity.this, AlertActivityShow.class);
			k.putExtra(Constants.ALERT_ID_PARCEL_STRING, dparcelable);
			startActivity(k);

		    } catch(Exception e) {
			Log.e(LOGGER_TAG, "Exception: " + e.toString());
		    }    

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception opening database: " + e.toString());
		}
	    }
	});

    }  

    @Override
    protected void onPause() {

	try{    

	    datasource.close();
	    place_datasource.close();

	}catch(Exception e){
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception onPause: " + e.toString());
	}

	super.onPause();
    }

    @Override
    protected void onResume() {
	datasource.open();

	values = datasource.getAllAlerts();
	adapter = new AlertAdapter(this, R.layout.places_main_item_list, values);
	setListAdapter(adapter);

	datasource.close();

	super.onResume();
    }

    /**
     * Shows an alert dialog asking the user to activate the GPS before to continue  
     * 
     * @author jcancela
     */
    public void showGPSAlert(){	    
	//Alert message
	alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
	alertDialog.setTitle(Tools.getStringResource(R.string.alert_dialog_gps_title));
	alertDialog.setMessage(Tools.getStringResource(R.string.alert_dialog_gps_message));

	alertDialog.setCancelable(false);

	alertDialog.setButton(Tools.getStringResource(R.string.alert_dialog_gps_activate), new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		AlertsActivity.this.startActivity(intent);
	    }
	});

	alertDialog.setButton2(Tools.getStringResource(R.string.alert_dialog_gps_cancel), new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		// Go back to edition form
		alertDialog.cancel();
	    }
	});

	alertDialog.show();
    }

    /**
     * Helps to customize the ListView
     * 
     * @author jcancela
     *
     */
    private class AlertAdapter extends ArrayAdapter<Alert> {
	private List<Alert> alerts;

	private AlertAdapter(Context context, int view, List<Alert> myAlerts) {
	    super(context, view, myAlerts);
	    alerts = myAlerts;
	}

	@Override
	public int getCount() {
	    return alerts.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    View currentView = convertView;
	    final ToggleButton toogle;

	    try {

		LayoutInflater currentViewInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		currentView = currentViewInflater.inflate(R.layout.alerts_main_item_list, null);

		Alert currentAlert = alerts.get(position);

		toogle = (ToggleButton) currentView.findViewById(R.id.toggleButtonAlertsAdd);
		TextView textPlaceName = (TextView) currentView.findViewById(R.id.textViewItemListPlaceName);
		TextView textDistance = (TextView) currentView.findViewById(R.id.textViewItemListPlaceDistance);
		TextView textComment = (TextView) currentView.findViewById(R.id.textViewItemListPlaceComment);

		Place newPlace = currentAlert.getPlace();

		textPlaceName.setText(newPlace.getName());
		textComment.setText(currentAlert.toStringCommentLimited(30));

		if (Tools.lastGeoPoint != null){                     
		    textDistance.setText(Tools.getDistanceToPrint(Tools.lastGeoPoint, new GeoPoint(newPlace.getLat() / 1E6, newPlace.getLon() / 1E6)));
		}

		toogle.setChecked(currentAlert.getFlagActiveBoolean());		  

		final Alert myAlert = (Alert) lv.getAdapter().getItem(position);

		toogle.setOnClickListener(new OnClickListener()
		{
		    @Override
		    public void onClick(View v)
		    {
			if(toogle.isChecked()){	     

			    myAlert.setAlarmActive(true);   

			} else{

			    myAlert.setAlarmActive(false);

			}
		    }
		});

	    } catch (Exception e) {
		Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			LOGGER_TAG, "Exception on getView: " + e.toString());
	    }

	    return currentView;
	}
    }  
}
