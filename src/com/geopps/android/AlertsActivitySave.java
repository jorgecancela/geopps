package com.geopps.android;

import java.util.Calendar;
import java.util.TimeZone;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.geopps.android.db.AlertsDataSource;
import com.geopps.android.db.PlacesDataSource;
import com.geopps.android.utils.Alert;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.DataParcelable;
import com.geopps.android.utils.Place;
import com.geopps.android.utils.SelectAreaItemizedOverlay;
import com.geopps.android.utils.Tools;

/**
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/

public class AlertsActivitySave extends Activity {
    private Button addAlertButton, addRingtoneButton, addStartDateButton, addEndDateButton, addTimezone;
    private String chosenRingtoneUri, chosenRingtoneTitle, timezone;
    private AlertsDataSource alertsDatasource;
    private PlacesDataSource datasource;
    private AlertDialog.Builder timezoneAlertDialog;
    private TextView tvTitle, tvTime;
    private int currentAlertId = -1;
    private Place currentPlace;
    private EditText etComment;
    private MapView mapView;
    private AlertDialog alertDialog;
    private String LOGGER_TAG = "AlertsActivitySave";

    private Calendar startCalendar = Calendar.getInstance();  
    private Calendar endCalendar = Calendar.getInstance();  
    private int sYear = 0, sMonth = 0, sDay = 0, sHour = 0, sMinute = 0;
    private int eYear = 0, eMonth = 0, eDay = 0, eHour = 0, eMinute = 0;

    static final int TIME_DIALOG_ID = 999;
    static final int START_DATE_DIALOG_ID = 998;
    static final int END_DATE_DIALOG_ID = 997;
    static final int TIMEZONE_DIALOG_ID = 996;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	// Read the last configuration stored in the database and setup the user settings
	try {
	    
	    Tools.setSettings(this.getApplicationContext());
	    Tools.setContext(this.getApplicationContext());
	
	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception setting up context: " + e.toString());
	}

	// Initizalizate the Views
	setContentView(R.layout.alerts_add);

	mapView = (MapView) findViewById(R.id.mapViewAlertsAdd);
	mapView.setBuiltInZoomControls(true);

	etComment = (EditText) findViewById(R.id.editTextAlertsAdd);

	addRingtoneButton = (Button) findViewById(R.id.buttonRingtoneAdd);
	addAlertButton = (Button) findViewById(R.id.buttonSaveAdd);

	addStartDateButton = (Button) findViewById(R.id.buttonStartDay);
	addStartDateButton.setText(Tools.getStringResource(R.string.alerts_add_start_day_title)+ " " +Tools.getStringResource(R.string.alerts_add_start_day_content));

	addEndDateButton = (Button) findViewById(R.id.buttonEndDay);
	addEndDateButton.setText(Tools.getStringResource(R.string.alerts_add_end_day_title)+ " " +Tools.getStringResource(R.string.alerts_add_end_day_content));

	addTimezone = (Button) findViewById(R.id.buttonTimeZone);
	timezone = TimeZone.getDefault().getID();
	addTimezone.setText(timezone);	

	tvTitle = (TextView) findViewById(R.id.textViewAlertsAddTitle);	

	// Trying to read an Alert ID (this Activity can be called to edit and alert or to create a new one)
	try {

	    // Reading the data parcelable  
	    Intent i = getIntent();
	    DataParcelable currentAlertID = (DataParcelable) i.getParcelableExtra(Constants.ALERT_ID_PARCEL_STRING);

	    alertDialog = new AlertDialog.Builder(this.getApplicationContext()).create();
	    
	    alertsDatasource = new AlertsDataSource(this.getApplicationContext());
	    alertsDatasource.open();
	    
	    Alert currentAlert = alertsDatasource.getAlertById(currentAlertID.intParcelable);
	    
	    currentAlertId = (int) currentAlert.getId();
	    alertsDatasource.close();

	    currentPlace = currentAlert.getPlace();

	    tvTitle.setText(Tools.getStringResource(R.string.alerts_activity_save_edit) + " " + currentPlace.getName());

	    // Modificar esto
	    if(currentAlert.getEndDate().get(Calendar.YEAR) == Constants.MAX_CALENDAR_YEAR){
		addEndDateButton.setText(Tools.getStringResource(R.string.alerts_add_end_day_title)+ " " +Tools.getStringResource(R.string.alerts_add_end_day_content));		
	    }else{
		addEndDateButton.setText(Tools.getStringResource(R.string.alerts_add_end_day_title)+ " " +Tools.CalendarDateToStringToShow(currentAlert.getEndDate()));
	    }

	    if(Tools.isToday(currentAlert.getStartDate())){
		addStartDateButton.setText(Tools.getStringResource(R.string.alerts_add_start_day_title)+ " " +Tools.getStringResource(R.string.alerts_add_start_day_content));
	    }else{
		addStartDateButton.setText(Tools.getStringResource(R.string.alerts_add_start_day_title)+ " " +Tools.CalendarDateToStringToShow(currentAlert.getStartDate()));
	    }
	    
	    addTimezone.setText(currentAlert.getTimezone());
	    etComment.setText(currentAlert.getComment());

	    Uri uri;
    	    
	    if ((currentAlert.getRingtone() == null)||(currentAlert.getRingtone().compareTo("")==0)){	
		uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
	    }else{
		uri = Uri.parse(currentAlert.getRingtone());
	    }
	    
	    Ringtone ringtone = RingtoneManager.getRingtone(this.getApplicationContext(), uri); 
	    addRingtoneButton.setText(ringtone.getTitle(this.getApplicationContext()));

	} catch (Exception e) {
	    alertsDatasource.close();
	}


	try {

	    timezoneAlertDialog = new AlertDialog.Builder(AlertsActivitySave.this);

	    final String[] TZ = TimeZone.getAvailableIDs();

	    timezoneAlertDialog.setTitle(Tools.getStringResource(R.string.alerts_add_timezone));
	    timezoneAlertDialog.setSingleChoiceItems(TZ, 0,new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
		    // onClick Action
		    timezone = TZ[whichButton]; 
		    addTimezone.setText(timezone);
		}
	    })

	    .setPositiveButton(Tools.getStringResource(R.string.alerts_activity_save_dialog_button_ok), new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
		    dialog.cancel();
		}
	    });

	    datasource = new PlacesDataSource(this.getApplicationContext());

	    Intent i = getIntent();
	    DataParcelable currentPlaceID = (DataParcelable) i.getParcelableExtra(Constants.PLACE_ID_PARCEL_STRING);

	    if (currentPlace == null){
		datasource.open();
		currentPlace = datasource.getPlaceById(currentPlaceID.intParcelable);
		datasource.close();
	    }

	    Drawable marker = getResources().getDrawable(R.drawable.place);
	    ResourceProxy resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());

	    SelectAreaItemizedOverlay myItemizedOverlay = new SelectAreaItemizedOverlay(marker, resourceProxy);
	    mapView.getOverlays().add(myItemizedOverlay);

	    GeoPoint currentPoint = new GeoPoint(currentPlace.getLat() / 1E6, currentPlace.getLon() / 1E6);

	    myItemizedOverlay.setArea(currentPoint, currentPlace.getRadius());
	    myItemizedOverlay.addItem(currentPoint, currentPlace.getName(), currentPlace.getName());

	    mapView.getController().setZoom(Tools.calculateZoomLevel(currentPlace.getRadius()));
	    mapView.getController().setCenter(currentPoint);
	    mapView.invalidate();

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception opening database: " + e.toString());
	}

	addAlertButton.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {

		    if (sYear == 0){
			startCalendar = Calendar.getInstance();
		    }else{
			startCalendar.set(sYear, sMonth, sDay, sHour, sMinute);
		    }

		    if(eYear == 0){
			endCalendar.set(Calendar.YEAR, Constants.MAX_CALENDAR_YEAR);
		    }else{
			endCalendar.set(eYear, eMonth, eDay, eHour, eMinute);
		    }
		    	    
		    if(startCalendar.after(endCalendar)){

			showAlertMessage(Tools.getStringResource(R.string.add_alert_dialog_message));

		    }else{
			
			TimeZone tz = TimeZone.getTimeZone(timezone);
			startCalendar.setTimeZone(tz);
			endCalendar.setTimeZone(tz);

			if (currentAlertId == -1){
			    alertsDatasource.createAlert(Tools.CalendarToStringToDb(startCalendar), Tools.CalendarToStringToDb(endCalendar), 
				    timezone, etComment.getText().toString(), chosenRingtoneUri, Constants.BOOLEAN_TRUE,
				    Constants.ALERT_TYPE_ALARM, (int) currentPlace.getId());
			}else{
			    alertsDatasource.updateAlert(currentAlertId, Tools.CalendarToStringToDb(startCalendar), Tools.CalendarToStringToDb(endCalendar), 
				    timezone, etComment.getText().toString(), chosenRingtoneUri, Constants.BOOLEAN_TRUE,
				    Constants.ALERT_TYPE_ALARM, (int) currentPlace.getId());
			}

			Intent k = new Intent(AlertsActivitySave.this, AlertsActivity.class);
			startActivity(k);

		    }

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception: " + e.toString());
		}
	    }
	});

	addRingtoneButton.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {

		    Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
		    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
		    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, Tools.getStringResource(R.string.alerts_activity_save_select_ringtone_title));
		    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
		    startActivityForResult(intent, 5);

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception: " + e.toString());
		}

	    } 
	});

	addStartDateButton.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {

		    showDialog(START_DATE_DIALOG_ID);

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception: " + e.toString());
		}
	    } 
	});

	addEndDateButton.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {

		    showDialog(END_DATE_DIALOG_ID);

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception: " + e.toString());
		}
	    } 
	});

	addTimezone.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {

		    showDialog(TIMEZONE_DIALOG_ID);

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception: " + e.toString());
		}
	    } 
	});

    }

    @Override
    protected Dialog onCreateDialog(int id) {

	Calendar privateCalendar = Calendar.getInstance();

	switch (id) {
	case TIME_DIALOG_ID:
	    return new TimePickerDialog(this, startTimePickerListener, sHour, sMinute, false);

	case START_DATE_DIALOG_ID:
	    return new DatePickerDialog(this, startDateSetListener, privateCalendar.get(Calendar.YEAR),  privateCalendar.get(Calendar.MONTH), privateCalendar.get(Calendar.DAY_OF_MONTH));

	case END_DATE_DIALOG_ID:
	    return new DatePickerDialog(this, endDateSetListener,privateCalendar.get(Calendar.YEAR), privateCalendar.get(Calendar.MONTH), privateCalendar.get(Calendar.DAY_OF_MONTH));

	case TIMEZONE_DIALOG_ID:
	    timezoneAlertDialog.show();

	}

	return null;
    }

    private TimePickerDialog.OnTimeSetListener startTimePickerListener = new TimePickerDialog.OnTimeSetListener() {
	public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
	    sHour = selectedHour;
	    sMinute = selectedMinute;

	    Calendar privateCalendar = Calendar.getInstance();
	    privateCalendar.set(0, 0, 0, sHour, sMinute);

	    tvTime.setText(Tools.CalendarTimeToStringToShow(privateCalendar));
	}
    };

    private DatePickerDialog.OnDateSetListener startDateSetListener = new DatePickerDialog.OnDateSetListener() {
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
	    sYear = year;
	    sMonth = monthOfYear;
	    sDay = dayOfMonth;

	    Calendar privateCalendar = Calendar.getInstance();
	    privateCalendar.set(year, monthOfYear, dayOfMonth, 0, 0);

	    addStartDateButton.setText(Tools.getStringResource(R.string.alerts_add_start_day_title)+ " " +Tools.CalendarDateToStringToShow(privateCalendar));	
	}
    };

    private DatePickerDialog.OnDateSetListener endDateSetListener = new DatePickerDialog.OnDateSetListener() {
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
	    eYear = year;
	    eMonth = monthOfYear;
	    eDay = dayOfMonth;

	    Calendar privateCalendar = Calendar.getInstance();
	    privateCalendar.set(year, monthOfYear, dayOfMonth, 23, 59);

	    addEndDateButton.setText(Tools.getStringResource(R.string.alerts_add_end_day_title)+ " " +Tools.CalendarDateToStringToShow(privateCalendar));	
	}
    };

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent)
    {
	if (resultCode == Activity.RESULT_OK && requestCode == 5)
	{
	    Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

	    if (uri != null)
	    {

		this.chosenRingtoneUri = uri.toString();

		Ringtone ringtone = RingtoneManager.getRingtone(this.getApplicationContext(), uri);
		this.chosenRingtoneTitle =  ringtone.getTitle(this.getApplicationContext());

		addRingtoneButton.setText(this.chosenRingtoneTitle);

	    } else {

		this.chosenRingtoneUri = null;
		this.chosenRingtoneTitle = "default";

	    }
	}            
    }

    private void showAlertMessage(String message) {
	// Alert message
	alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
	alertDialog.setTitle(Tools.getStringResource(R.string.add_alert_dialog_title));
	alertDialog.setMessage(message);

	alertDialog.setCancelable(false);

	alertDialog.setButton(Tools.getStringResource(R.string.add_alert_dialog_button), new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		dialog.cancel();
	    }
	});

	alertDialog.show();
    }

    @Override 
    protected void onPause() {
	try {

	    // Just in case
	    datasource.close();
	    alertsDatasource.close();

	} catch (Exception e) { 
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception: " + e.toString());
	}

	super.onPause();
    }

    /**
    @Override
    protected void onResume() {

	try {

	    datasource = new PlacesDataSource(this.getApplicationContext());
	    datasource.open();

	    alertsDatasource = new AlertsDataSource(this.getApplicationContext());
	    alertsDatasource.open();

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception opening database: " + e.toString());
	}

	super.onResume();
    }
     */
}
