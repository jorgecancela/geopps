package com.geopps.android;

import java.util.ArrayList;
import java.util.List;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.bonuspack.overlays.DefaultInfoWindow;
import org.osmdroid.bonuspack.overlays.ExtendedOverlayItem;
import org.osmdroid.bonuspack.overlays.ItemizedOverlayWithBubble;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemClickListener;

import com.geopps.android.db.AlertsDataSource;
import com.geopps.android.utils.Alert;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.DataParcelable;
import com.geopps.android.utils.Place;
import com.geopps.android.utils.SelectAreaItemizedOverlay;
import com.geopps.android.utils.Tools;


/**
 * 
 * Shows the user position in real time and a bunch of alerts. By the default if will show 
 * all the active alerts. It is possible to pass a Int flag:
 * 
 *  MAP_TYPE_ID_PARCEL_STRING:
 *  
 *  SHOW_ACTIVE_ALERTS = 1;
 *  SHOW_ALL_ALERTS = 2;
 *  SHOW_RAISED_ALERTS = 3;
 * 
 * In this case, we will stop the global tracking service and we will use
 * a private tracker (in a different Runnable). The map will also show the
 * active alerts.
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class MapShowActivity extends ListActivity {
    private SelectAreaItemizedOverlay myUserOverlay = null;
    private AlertsDataSource datasource ;
    private Drawable uMarker;
    private MapView mapView;
    private int mapType;
    private TextView tvTitle;
    private ArrayAdapter<Alert> adapter;
    private List<GeoPoint> gpList = null;
    private List<Alert> values = null;
    private ListView lv;
    private String LOGGER_TAG = "MapShowActivity";
    public RealTimeUserTracker myRealTimeUserTracker;
    public boolean firstTime = true;

    @Override
    protected void onPause() {
	try {

	    myRealTimeUserTracker.stop();
	    datasource.close();

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception onPause: " + e.toString());
	}
	super.onPause();
    }

    @Override
    protected void onResume() {
	try {

	    if (!myRealTimeUserTracker.running){
		myRealTimeUserTracker.execute();
	    }
	    
	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception onResume: " + e.toString());
	}
	super.onResume();
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	// Read the last configuration stored in the database and setup the configuration
	try {

	    Tools.setSettings(this.getApplicationContext());
	    Tools.setContext(this.getApplicationContext());

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception setting up context: " + e.toString());
	}

	setContentView(R.layout.map_show);

	mapView = (MapView) findViewById(R.id.mapViewMapShow);
	mapView.setBuiltInZoomControls(true);
	mapView.setClickable(true);
	mapView.setMultiTouchControls(true);

	tvTitle = (TextView) findViewById(R.id.textViewMapShowTitle); 	

	try {

	    Intent i = getIntent();
	    DataParcelable currentAlertID = (DataParcelable) i.getParcelableExtra(Constants.MAP_TYPE_ID_PARCEL_STRING);
	    mapType = currentAlertID.intParcelable;

	    ResourceProxy resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());
	    uMarker = getResources().getDrawable(R.drawable.marker2);

	    myUserOverlay = new SelectAreaItemizedOverlay(uMarker, resourceProxy);
	    myUserOverlay.setTouchable(false);
	    myUserOverlay.setBottomCenter(true);
	    mapView.getOverlays().add(myUserOverlay);

	    datasource = new AlertsDataSource(this.getApplicationContext());

	    switch(mapType) {

	    case Constants.SHOW_RAISED_ALERTS:
		// stop alerts and notifications
		stopAlertsAndNotifications();

		values = Tools.alertsRaised;
		gpList = addAlertsToMapView(Tools.alertsRaised);
		tvTitle.setText(Tools.getStringResource(R.string.map_show_activity_alerts_raised));
		break;

	    case Constants.SHOW_ALL_ALERTS:
		datasource.open();
		values = datasource.getAllAlerts();
		gpList = addAlertsToMapView(values);
		datasource.close();
		tvTitle.setText(Tools.getStringResource(R.string.map_show_activity_all_alerts));
		break;

	    default:
		datasource.open();
		values = datasource.getAllActiveAlerts();
		gpList = addAlertsToMapView(values);
		datasource.close();
		tvTitle.setText(Tools.getStringResource(R.string.map_show_activity_active_alerts));

	    }

	    // ExtendedOverlayItem is a special OverlayItem (included in the Osmdroid bonuspack) which allows
	    // to create Items that will show a bubble when touched
	    final ArrayList<ExtendedOverlayItem> osmPois = new ArrayList<ExtendedOverlayItem>();

	    for (int j = 0; j < values.size(); j++) {
		ExtendedOverlayItem point = new ExtendedOverlayItem("title", "", gpList.get(j), this.getApplicationContext());

		// We link each marker in the map with an Alert object
		point.setRelatedObject(values.get(j));

		point.setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
		point.setMarker(getResources().getDrawable(R.drawable.place));

		point.setTitle(values.get(j).getPlace().getName());
		point.setDescription(values.get(j).toStringCommentLimited(Constants.TEXT_LIMITED_20_CHARS));

		osmPois.add(point);
	    }

	    ItemizedOverlayWithBubble<ExtendedOverlayItem> osmPoisOverlay = new ItemizedOverlayWithBubble<ExtendedOverlayItem>(this.getApplicationContext(), osmPois, mapView, new OsmInfoBubble(mapView));
	    mapView.getOverlays().add(osmPoisOverlay);

	    // fix point
	    if(Tools.lastGeoPoint != null){

		gpList.add(Tools.lastGeoPoint);

	    }

	    myUserOverlay.clear();
	    myUserOverlay.addItem(Tools.lastGeoPoint, "","");

	    ArrayList<GeoPoint> geoPointsToShow = new ArrayList<GeoPoint>();	    
	    geoPointsToShow.addAll(gpList);

	    BoundingBoxE6 bbE = BoundingBoxE6.fromGeoPoints(geoPointsToShow); 

	    mapView.getController().setCenter(bbE.getCenter());
	    mapView.zoomToBoundingBox(bbE);
	    mapView.invalidate();		

	    // Este gpList incluye la poscion de usuario u no deber�a
	    myRealTimeUserTracker = new RealTimeUserTracker();

	    // myRealTimeUserTracker.setupTracker(myUserOverlay, mapView, gpList, true, true);
	    myRealTimeUserTracker.execute();

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception populating the map: " + e.toString());
	}

	adapter = new AlertAdapter(this, R.layout.places_main_item_list, values);
	setListAdapter(adapter);

	lv = getListView();
	lv.setTextFilterEnabled(true);

	// Warning! : Inside this lv we have a ToggleButton which also has an onClickListener, it makes that 
	// this lv onClickListener doesn'r work unless we add this line  android:focusable="false"
	// in the ToggleButton xml file
	lv.setOnItemClickListener(new OnItemClickListener() {
	    @Override
	    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		try {

		    Alert alert = (Alert) lv.getAdapter().getItem(position);

		    try {	

			DataParcelable dparcelable = new DataParcelable((int) alert.getId());  

			Intent k = new Intent(MapShowActivity.this, AlertActivityShow.class);
			k.putExtra(Constants.ALERT_ID_PARCEL_STRING, dparcelable);
			startActivity(k);

		    } catch(Exception e) {
			Log.e(LOGGER_TAG, "Error: " + e.toString());
		    }    

		} catch (Exception e) {
		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Exception opening database: " + e.toString());
		}
	    }
	});	
    }

    private List<GeoPoint> addAlertsToMapView(List<Alert> values){
	List<GeoPoint> gpValues = new ArrayList<GeoPoint>();

	for (Alert a : values){

	    GeoPoint gp = new GeoPoint(a.getPlace().getLat() / 1E6, a.getPlace().getLon() / 1E6);	    
	    gpValues.add(gp);

	}

	return gpValues;
    }   

    private void stopAlertsAndNotifications(){

	if (AlertDialogActivity.isRinging()){

	    AlertDialogActivity.setIsRinging(false);

	    try{

		AlertDialogActivity.mManager.cancel(AlertDialogActivity.APP_ID_NOTIFICATION);

	    }catch(Exception e){
		Log.e(LOGGER_TAG, "Exception Not e: " + e.toString());
	    }

	    try{

		Tools.alertsRaised = (ArrayList<Alert>) AlertDialogActivity.stopAlerts();

	    }catch(Exception e){
		Log.e(LOGGER_TAG, "Exception StopA e: " + e.toString());
	    }

	}
    }


    // Helps to customize the ListView
    private class AlertAdapter extends ArrayAdapter<Alert> {
	private List<Alert> alerts;

	public AlertAdapter(Context context, int view, List<Alert> myAlerts) {
	    super(context, view, myAlerts);
	    alerts = myAlerts;
	}

	@Override
	public int getCount() {
	    return alerts.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    View currentView = convertView;
	    final ToggleButton toogle;

	    try {

		LayoutInflater currentViewInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		currentView = currentViewInflater.inflate(R.layout.alerts_main_item_list, null);

		Alert currentAlert = alerts.get(position);

		toogle = (ToggleButton) currentView.findViewById(R.id.toggleButtonAlertsAdd);
		TextView textPlaceName = (TextView) currentView.findViewById(R.id.textViewItemListPlaceName);
		TextView textDistance = (TextView) currentView.findViewById(R.id.textViewItemListPlaceDistance);
		TextView textComment = (TextView) currentView.findViewById(R.id.textViewItemListPlaceComment);

		Place newPlace = currentAlert.getPlace();

		textPlaceName.setText(newPlace.getName());
		textComment.setText(currentAlert.toStringCommentLimited(Constants.TEXT_LIMITED_50_CHARS));

		if (Tools.lastGeoPoint != null){
		    textDistance.setText(Tools.getDistanceToPrint(Tools.lastGeoPoint, new GeoPoint(newPlace.getLat() / 1E6, newPlace.getLon() / 1E6)));
		}

		if (currentAlert.getFlagActiveBoolean()) {
		    toogle.setChecked(true);		  
		} else {
		    toogle.setChecked(false);
		}

		final Alert myAlert = (Alert) lv.getAdapter().getItem(position);

		toogle.setOnClickListener(new OnClickListener()
		{
		    @Override
		    public void onClick(View v)
		    {
			if(toogle.isChecked()){	     
			    myAlert.setAlarmActive(true);
			} else{
			    myAlert.setAlarmActive(false);
			}
		    }
		});

	    } catch (Exception e) {
		Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			LOGGER_TAG, "Exception in getView: " + e.toString());
	    }

	    return currentView;
	}
    }  

    private class RealTimeUserTracker extends AsyncTask<Void, String, Void> {

	private boolean running = true;

	@Override
	protected Void doInBackground(Void... params) {
	    while( running ) {

		try {
		    publishProgress();
		    Thread.sleep(10000);
		} catch (Exception e) {
		    Log.v(LOGGER_TAG, "Exception e: "  + e.toString());
		} 
	    }

	    return null;
	}

	@Override
	protected void onProgressUpdate(String... values) {

	    BoundingBoxE6 bbE = Tools.updateMapView(myUserOverlay, mapView, gpList, true, firstTime);	

	    mapView.getController().setCenter(bbE.getCenter());

	    if (firstTime){
		mapView.zoomToBoundingBox(bbE);
	    }else{

	    }

	    mapView.invalidate();

	    firstTime = false;

	    if( ! running ) {
		return; 
	    }
	}

	public void stop() {
	    running = false;
	}
    }

    // Helps to customize the Info Bubbles in the map
    // When the user taps on a icon a OsmInfoBuble will be shown
    private class OsmInfoBubble extends DefaultInfoWindow {
	Alert alert;

	public OsmInfoBubble(MapView mapView) {
	    super(R.layout.bonuspack_bubble, mapView);

	    Button btn = (Button) (mView.findViewById(R.id.bubble_moreinfo));

	    btn.setOnClickListener(new View.OnClickListener() {
		public void onClick(View v) {

		    DataParcelable dparcelable = new DataParcelable((int) alert.getId()); 
		    Intent k = new Intent(Tools.getContext(), AlertActivityShow.class);
		    k.putExtra(Constants.ALERT_ID_PARCEL_STRING, dparcelable);
		    startActivity(k);

		}
	    });
	}

	@Override
	public void onOpen(ExtendedOverlayItem item) {
	    super.onOpen(item);

	    try{

		alert = (Alert) item.getRelatedObject();
		mView.findViewById(R.id.bubble_moreinfo).setVisibility(View.VISIBLE);

	    }catch(Exception e){
		Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			LOGGER_TAG, "Exception in OsmInfo: " + e.toString());
	    }
	}
    }
}
