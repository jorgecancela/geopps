package com.geopps.android;

import java.util.ArrayList;
import java.util.List;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.geopps.android.db.PlacesDataSource;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.DataParcelable;
import com.geopps.android.utils.Place;
import com.geopps.android.utils.Tools;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class PlacesActivity extends ListActivity {
    private PlacesDataSource datasource;
    private ArrayAdapter<Place> adapter;
    private ArrayList<Place> adapter_sorted;
    private Button addPlaceButton;
    private List<Place> values;
    private static ListView lv;
    private EditText et;
    private String LOGGER_TAG = "PlacesActivity";
 
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	
	// Read the last configuration stored in the database and setup the configuration	
	try {
	    
	    Tools.setSettings(this.getApplicationContext());
	    Tools.setContext(this.getApplicationContext());
	    
	} catch(Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Error: " + e.toString());
	}
	
	setContentView(R.layout.places_main);

	addPlaceButton = (Button) findViewById(R.id.placesMainAddNewPlaceButton);
	addPlaceButton.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {
		    
		    datasource.close();
		    Intent k = new Intent(PlacesActivity.this, PlacesActivityAdd.class);
		    startActivity(k);
		    
		} catch (Exception e) {
		    Log.e(LOGGER_TAG, "Error: " + e.toString());
		}
	    }
	});

	try {
	    
	    datasource = new PlacesDataSource(this.getApplicationContext());
	    datasource.open();
	    values = datasource.getAllPlaces();

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Error: " + e.toString());
	}
	
	adapter = new PlacesAdapter(this, R.layout.places_main_item_list, values);
	
	adapter_sorted = new ArrayList<Place>();
	
	setListAdapter(adapter);

	lv = getListView();
	lv.setTextFilterEnabled(true);

	lv.setOnItemClickListener(new OnItemClickListener() {
	    @Override
	    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	    Place place = (Place) lv.getAdapter().getItem(position);

		try {			         	 
		    DataParcelable dparcelable = new DataParcelable((int) place.getId());  
		    datasource.close();

		    // When a element on the List is pressed ExplorePatient is called with the patient ID as argument
		    //Intent k = new Intent(PlacesActivity.this, AlertsActivitySave.class);
		    Intent k = new Intent(PlacesActivity.this, PlacesActivityShow.class);
		    k.putExtra(Constants.PLACE_ID_PARCEL_STRING, dparcelable);
		    startActivity(k);

		} catch(Exception e) {
		    Log.v(LOGGER_TAG, "Error: " + e.toString());
		}
	  
	    }
	});
	
	//
	// et is the search field, when the user starts typing this listener looks for patients in the list who match
	// and the ListView is automatically updated just with this patients
	//
	et = (EditText) findViewById(R.id.etPlacesMainSearch);
	
	et.addTextChangedListener(new TextWatcher()
	{
	    @Override
	    public void afterTextChanged(Editable s) {
		// Abstract Method of TextWatcher Interface.
	    }

	    @Override
	    public void beforeTextChanged(CharSequence s,
		    int start, int count, int after) {
		// Abstract Method of TextWatcher Interface.
	    }

	    @Override
	    public void onTextChanged(CharSequence s,
		    int start, int before, int count) {
		//textlength = et.getText().length();
		adapter_sorted.clear();
	
		 datasource.open();

		 for(Place p : datasource.getAllPlaces()) {
		     if((p.toString().toLowerCase().contains(s.toString().toLowerCase()))||(s.toString().compareTo("")==0)) {
			 adapter_sorted.add(p);
		     }
		}
		    datasource.close();

		adapter.setNotifyOnChange(true);
		adapter.clear();
		
		for (Place p : adapter_sorted) {
		    adapter.add(p);
		    adapter.notifyDataSetChanged();
		}

		lv.setAdapter(adapter);
	    }
	});
	

    }
    
    protected void onPause() {

	try{    
	
	    datasource.close();
	
	}catch(Exception e){
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception: " + e.toString());
	}
	super.onPause();
    }



    @Override
    protected void onResume() {
	datasource.open();
	super.onResume();
    }
    
    
    /**
     * Helps to customize the ListView
     * 
     * @author jcancela
     *
     */
    private class PlacesAdapter extends ArrayAdapter<Place> {
	private List<Place> places;

	private PlacesAdapter(Context context, int view, List<Place> myPlaces) {
	    super(context, view, myPlaces);
	    places = myPlaces;
	}

	@Override
	public int getCount() {
	    return places.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    View currentView = convertView;

	    try {

		LayoutInflater currentViewInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		currentView = currentViewInflater.inflate(R.layout.places_main_item_list, null);

		Place currentPlace = places.get(position);

		TextView textPlaceName = (TextView) currentView.findViewById(R.id.textViewItemPlacesName);
		TextView textInfo = (TextView) currentView.findViewById(R.id.textViewItemPlacesInfo);

		textPlaceName.setText(currentPlace.getName());
		textInfo.setText(Tools.getRadiousToPrint(currentPlace.getRadius()));

	    } catch (Exception e) {
		Log.e(LOGGER_TAG, "Exception on getView: " + e.toString());
	    }

	    return currentView;
	}
    }  
}
