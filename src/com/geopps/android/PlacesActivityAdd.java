package com.geopps.android;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.geopps.android.db.PlacesDataSource;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.DataParcelable;
import com.geopps.android.utils.Place;
import com.geopps.android.utils.SelectAreaItemizedOverlay;
import com.geopps.android.utils.Tools;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class PlacesActivityAdd extends Activity {
    private Button searchButton, saveButton, addMarkerButton, showSearchButton, myLocationButton;
    private SelectAreaItemizedOverlay myItemizedOverlay = null;
    private static LinearLayout searchPanel;
    private static LinearLayout sizePanel;
    private MapController mapController;
    private EditText etName;
    private AutoCompleteTextView searchText;
    private MapView mapView;
    private Drawable uMarker, marker;
    private AlertDialog alertDialog;
    private ArrayAdapter<String> adapter;
    private int lastProgressValue = 0;
    private String LOGGER_TAG = "PlacesActivityAdd";
    private JSONArray searchResults = null;
    
    private GeoPoint point;

    public SelectAreaItemizedOverlay myUserOverlay = null;
    public static final int SEARCH_ID = Menu.FIRST;
    public static GeoPoint currentPoint;
    public static int currentRadius = Constants.X_LARGE_RADIUS/2;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	searchPanel = (LinearLayout) findViewById(R.id.searchPlacePanel);
	sizePanel = (LinearLayout) findViewById(R.id.addNewPlacePanel);

	addMarkerButton = (Button) findViewById(R.id.showAddNewPlaceButton);
	showSearchButton = (Button) findViewById(R.id.showSearchPanelButton);
	myLocationButton = (Button) findViewById(R.id.showMyLocationButton);

	saveButton = (Button) findViewById(R.id.addNewPlaceSavePlaceButton);
	searchButton = (Button) findViewById(R.id.searchPlaceButton);

	etName = (EditText) findViewById(R.id.etAddPlaceName);
	searchText = (AutoCompleteTextView) findViewById(R.id.searchPlaceText);

	mapView = (MapView) findViewById(R.id.mapView);

	mapView.setBuiltInZoomControls(true);
	mapView.setMultiTouchControls(true);

	mapController = mapView.getController();

	mapController.setZoom(Constants.SMALL_ZOOM);
	currentRadius = Constants.SMALL_RADIUS;

	ResourceProxy resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());

	uMarker = getResources().getDrawable(R.drawable.marker2);
	uMarker.setBounds(0, -uMarker.getIntrinsicHeight(), 0, 0);
	    
	myUserOverlay = new SelectAreaItemizedOverlay(uMarker, resourceProxy);  
	myUserOverlay.setBottomCenter(true);
	myUserOverlay.setTouchable(false);
	mapView.getOverlays().add(myUserOverlay);

	alertDialog = new AlertDialog.Builder(PlacesActivityAdd.this).create();

	// Center the map in the current user position
	if (Tools.lastGeoPoint != null) {
	    try {

		myUserOverlay.clear();
		myUserOverlay.addItem(Tools.lastGeoPoint, "","");

		mapController.setCenter(Tools.lastGeoPoint);

	    } catch (Exception e) {

		Log.e(LOGGER_TAG, "Error locating initial GeoPoint: " + e.getMessage());

	    }
	}

	// Continues with the rest of the map initialization
	try {

	    marker = getResources().getDrawable(R.drawable.place);
	    myItemizedOverlay = new SelectAreaItemizedOverlay(marker, resourceProxy);
	    mapView.getOverlays().add(myItemizedOverlay);

	} catch (Exception e) {

	    Log.e(LOGGER_TAG, "Exception: " + e.getMessage());

	}

	saveButton.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View view) {

		// Validate text box
		String name = etName.getText().toString().trim();

		try {

		    if (validateData(name)) {

			PlacesDataSource datasource = new PlacesDataSource(getApplicationContext());

			datasource.open();
			Place newPlace = datasource.createPlace(name, "", currentPoint.getLatitudeE6(), currentPoint.getLongitudeE6(), currentRadius);
			DataParcelable dparcelable = new DataParcelable((int) newPlace.getId());
			datasource.close();

			Intent k = new Intent(PlacesActivityAdd.this, AlertsActivitySave.class);

			k.putExtra(Constants.PLACE_ID_PARCEL_STRING, dparcelable);
			startActivity(k);
		    }

		} catch (Exception e) {

		    Log.e(LOGGER_TAG, "Exception: " + e.getMessage());

		}
	    }
	});

	final SeekBar mSeekBar=(SeekBar) findViewById(R.id.slider);

	mSeekBar.setMax(Constants.X_LARGE_RADIUS);
	mSeekBar.setProgress(Constants.X_LARGE_RADIUS/2);
	mSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

	    @Override
	    public void onStopTrackingTouch(SeekBar seekBar) {}

	    @Override
	    public void onStartTrackingTouch(SeekBar seekBar) {}

	    @Override
	    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

		hideSearchPanel();

		if(!patientIsEditingZoom(progress)){
		    adjustZoomLevelWithProgress(progress);
		}

		addRegionToMapView(progress, false);
		currentRadius = progress;
	    }
	});

	searchButton.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View view) {

		try {

		    String searchFor = searchText.getText().toString();
		    //String[] inputString = { searchFor };
		    //SearchLocation mySearcher = new SearchLocation();
		    //mySearcher.execute(inputString);

		   // if(result != null)
		    
                   point = Tools.getGeoPointOfAddress(searchResults, searchFor);
		    
		    if(point != null){
			
			mapController.setZoom(Constants.SMALL_ZOOM);

			mapController.animateTo(point);

			changeSearchPanelToAddPanel();
			hideSearchPanel();

			currentPoint = (GeoPoint) mapView.getMapCenter();
			addRegionToMapView(Constants.SMALL_RADIUS, true);
			currentRadius = Constants.SMALL_RADIUS;

			mapView.invalidate();
			
			
		   }else{

			//Tools.showToastMessageAndLog(Tools.getContext(), "Place not found. Plase try again...",
			//	LOGGER_TAG, "Error searching");

		    }
		    
		    //currentPoint = mySearcher.get();

		} catch (Exception e) {

		    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
			    LOGGER_TAG, "Error: " + e.toString());

		}
	    }
	});

	addMarkerButton.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View view) {
		changeSearchPanelToAddPanel();
		hideSearchPanel();

		if (myItemizedOverlay != null) {

		    addRegionToMapView((int) currentRadius, true);

		    myItemizedOverlay.clear();
		    currentPoint = (GeoPoint) mapView.getMapCenter();

		    myItemizedOverlay.setArea(currentPoint, (int) currentRadius);
		    myItemizedOverlay.addItem(currentPoint, "", "");

		    mapView.invalidate();

		}
	    }
	});

	showSearchButton.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View view) {

		changeAddPanelToSearchPanel();

	    }
	});

	myLocationButton.setOnClickListener(new View.OnClickListener() {
	    public void onClick(View view) {

		if (Tools.lastGeoPoint != null) {
		    try {

			mapController.setZoom(Constants.SMALL_ZOOM);
			mapController.animateTo(Tools.lastGeoPoint);

		    } catch (Exception e) {

			Log.e(LOGGER_TAG, "Error locating initial GeoPoint: " + e.getMessage());

		    }
		}
	    }
	});

	
	try {

	    changeAddPanelToSearchPanel();
	    
	    adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line);
	    
	    adapter.setNotifyOnChange(true);
	    searchText.setAdapter(adapter);
	    
	    searchText.addTextChangedListener(new TextWatcher() {

		    @Override
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
			    adapter.clear();
			    
			    String searchFor = searchText.getText().toString();
			    String[] inputString = { searchFor };

			    SearchLocation task = new SearchLocation();
			    task.execute(inputString);
		    }

		    @Override
		    public void beforeTextChanged(CharSequence s, int start, int count,
			    int after) { 
		    }

		    @Override
		    public void afterTextChanged(Editable s) { }
		});


	} catch (Exception e) {
	    Log.e(LOGGER_TAG, "Exception: " + e.getMessage());
	}
	
    }

    private Boolean patientIsEditingZoom(int progressValue) {
	Boolean returnValue = false;

	if ((mapView.getZoomLevel() != Constants.SMALL_ZOOM)|| (mapView.getZoomLevel() != Constants.MEDIUM_ZOOM)|| (mapView.getZoomLevel() != Constants.LARGE_ZOOM)) {
	    returnValue = true;

	    if (lastProgressValue < (Constants.X_LARGE_RADIUS / 3)
		    && progressValue > (Constants.X_LARGE_RADIUS / 3)) {
		returnValue = false;
	    }

	    if (lastProgressValue > (Constants.X_LARGE_RADIUS / 3)
		    && progressValue < (Constants.X_LARGE_RADIUS / 3)) {
		returnValue = false;
	    }

	    if ((lastProgressValue < (2 * Constants.X_LARGE_RADIUS / 3))
		    && (progressValue > (2 * Constants.X_LARGE_RADIUS / 3))) {
		returnValue = false;
	    }

	    if ((lastProgressValue > (2 * Constants.X_LARGE_RADIUS / 3))
		    && (progressValue < (2 * Constants.X_LARGE_RADIUS / 3))) {
		returnValue = false;
	    }
	}

	lastProgressValue = progressValue;
	return returnValue;
    }

    private void adjustZoomLevelWithProgress(int progressValue){
	if(progressValue < (Constants.X_LARGE_RADIUS/3)){
	    mapController.setZoom(Constants.SMALL_ZOOM);
	    mapController.setCenter(currentPoint);
	}

	if((progressValue < (2*Constants.X_LARGE_RADIUS/3))&&(progressValue > (Constants.X_LARGE_RADIUS/3))){
	    mapController.setZoom(Constants.MEDIUM_ZOOM);
	    mapController.setCenter(currentPoint);
	}

	if(progressValue > (2*Constants.X_LARGE_RADIUS/3)){
	    mapController.setZoom(Constants.LARGE_ZOOM);
	    mapController.setCenter(currentPoint);
	}

    }

    private Boolean validateData(String placeName) {

	if (placeName.equals(null) || placeName.equals("")) {

	    showAlertMessage(Tools.getStringResource(R.string.add_places_alert_dialog_name_blank));
	    return false;

	} else {

	    // Validate place. Check if the place is already in the database
	    PlacesDataSource pSource = new PlacesDataSource(this.getApplicationContext());
	    pSource.open();
	    Place myPlace = pSource.searchPlace(placeName);
	    pSource.close();

	    // If null this patient is not in the database
	    if (myPlace == null) {

		return true;

	    } else {

		showAlertMessage(Tools.getStringResource(R.string.add_places_alert_dialog_already_exists));
		return false;

	    }
	}
    }

    private void showAlertMessage(String message) {
	// Alert message
	alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
	alertDialog.setTitle(Tools.getStringResource(R.string.add_places_alert_dialog_title));
	alertDialog.setMessage(message);

	alertDialog.setCancelable(false);

	alertDialog.setButton(Tools.getStringResource(R.string.add_places_alert_dialog_button), new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
		dialog.cancel();
	    }
	});

	alertDialog.show();
    }

    private Boolean addRegionToMapView(int radiusSize, Boolean setMapCenter) {

	if(setMapCenter){
	    myItemizedOverlay.clear();
	    currentPoint = (GeoPoint) mapView.getMapCenter();
	    myItemizedOverlay.addItem(currentPoint, "", "");
	}

	myItemizedOverlay.setArea(currentPoint, radiusSize);

	hideSearchPanel();
	changeSearchPanelToAddPanel();

	mapView.invalidate();
	return true;
    }

    public static void changeSearchPanelToAddPanel(){
	searchPanel.setVisibility(View.INVISIBLE);
	sizePanel.setVisibility(View.VISIBLE);
    }

    private static void changeAddPanelToSearchPanel(){
	sizePanel.setVisibility(View.INVISIBLE);
	searchPanel.setVisibility(View.VISIBLE);
    }

    public void hideSearchPanel() {
	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	imm.hideSoftInputFromWindow(searchText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    // Recibe como input un lugar para buscar y lo transforma en un GeoPoint
    public class SearchLocation extends AsyncTask<String[], Void, GeoPoint> {

	@Override
	protected GeoPoint doInBackground(String[]... params) {

	    String[] inputString = params[0];

	    JSONArray results = searchLocation(inputString[0]);

	   
	    try {

		searchResults = results;
		
		/**
		if (results.length() > 0) {

		    JSONObject firstResult = (JSONObject) results.get(0);
		    Double lat = firstResult.getDouble("lat");
		    Double lon = firstResult.getDouble("lon");

		    point = new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));
		}
*/		

	    } catch (Exception e) {

		Log.e(LOGGER_TAG, "Exception: " + e.getMessage());

	    }

	    return point;
	}

	@Override
	protected void onPostExecute(GeoPoint result) {

	    try {
		adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line);
		adapter.setNotifyOnChange(true);

		for (String string : Tools.JSONArray2ArrayList(searchResults)) {
		    adapter.add(string);
		    adapter.notifyDataSetChanged();
		}

		searchText.setAdapter(adapter);
		searchText.showDropDown();

	    } catch (Exception e) {
		// WARNING! Do  not lauch toast message within this AsyncTask
		Log.v("AddNewPatient", "Exception within LoadMadagascarLocation AsyncTask " + e.toString());
	    }
	    

	}

	public JSONArray searchLocation(String query) {
	    JSONArray results = new JSONArray();

	    try {

		query = URLEncoder.encode(query, "UTF-8");

	    } catch (UnsupportedEncodingException e) {

		return results;

	    }

	    String url = "http://nominatim.openstreetmap.org/search?";
	    url += "q=" + query;
	    url += "&format=json";

	    HttpGet httpGet = new HttpGet(url);
	    DefaultHttpClient httpClient = new DefaultHttpClient();

	    try {

		HttpResponse response = httpClient.execute(httpGet);
		String content = EntityUtils.toString(response.getEntity(), "utf-8");
		results = new JSONArray(content);

	    } catch (Exception e) {

		Log.e(LOGGER_TAG, "Exception: " + e.getMessage());

	    }

	    return results;
	}

	public void hideSearchPanel() {
	    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(searchText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

    }
}
