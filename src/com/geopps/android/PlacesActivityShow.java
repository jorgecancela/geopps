package com.geopps.android;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.geopps.android.db.PlacesDataSource;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.DataParcelable;
import com.geopps.android.utils.Place;
import com.geopps.android.utils.SelectAreaItemizedOverlay;
import com.geopps.android.utils.Tools;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class PlacesActivityShow extends Activity {
    private SelectAreaItemizedOverlay myItemizedOverlay = null;
    private PlacesDataSource datasource;
    private MapView mapView;
    private TextView tvName;
    private Drawable marker;
    private Place currentPlace;
    private Button addAlert, deletePlace;
    private DataParcelable currentPlaceID;
    private AlertDialog alertDialog;
    private String LOGGER_TAG = "PlacesActivityShow";

    @Override
    protected void onPause() {
	try {
	    
	    datasource.close();
	
	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception: " + e.toString());
	}
	super.onPause();
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	// Read the last configuration stored in the database and setup the
	// configuration
	try {
	    
	    Tools.setSettings(this.getApplicationContext());
	    Tools.setContext(this.getApplicationContext());
	    
	    alertDialog =  new AlertDialog.Builder(PlacesActivityShow.this).create();
	    
	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception setting up context: " + e.toString());
	}

	setContentView(R.layout.places_show);

	mapView = (MapView) findViewById(R.id.mapViewPlacesShow);
	mapView.setBuiltInZoomControls(true);
	mapView.setMultiTouchControls(true);

	tvName = (TextView) findViewById(R.id.textViewPlacesShowTitle);

	try {
	    
	    datasource = new PlacesDataSource(this.getApplicationContext());
	    datasource.open();

	    Intent i = getIntent();
	    currentPlaceID = (DataParcelable) i.getParcelableExtra(Constants.PLACE_ID_PARCEL_STRING);

	    currentPlace = datasource.getPlaceById(currentPlaceID.intParcelable);
	    	    
	    tvName.setText(currentPlace.getName());

	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.app_name),
		    LOGGER_TAG, "Error: " + e.toString()); 
	}

	try {
	    
	    marker = getResources().getDrawable(R.drawable.place);
	    ResourceProxy resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());
	    myItemizedOverlay = new SelectAreaItemizedOverlay(marker, resourceProxy);
	    mapView.getOverlays().add(myItemizedOverlay);

	    GeoPoint currentPoint = new GeoPoint(currentPlace.getLat() / 1E6, currentPlace.getLon() / 1E6);

	    mapView.getController().setZoom(Tools.calculateZoomLevel(currentPlace.getRadius()));

	    myItemizedOverlay.setArea(currentPoint, currentPlace.getRadius());
	    myItemizedOverlay.addItem(currentPoint, currentPlace.getName(), currentPlace.getName());

	    mapView.getController().setCenter(currentPoint);
	   
	    mapView.invalidate();
	    
	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception: " + e.toString());
	}
	
	addAlert = (Button) findViewById(R.id.buttonPlacesShowAddAlert);
	addAlert.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {
		    
		    datasource.close();
		    Intent k = new Intent(PlacesActivityShow.this, AlertsActivitySave.class);
		    k.putExtra(Constants.PLACE_ID_PARCEL_STRING, currentPlaceID);
		    startActivity(k);
		    
		} catch (Exception e) {
		    Log.e(LOGGER_TAG, "Error: " + e.toString());
		}
	    }
	});
	
	deletePlace = (Button) findViewById(R.id.buttonPlacesShowDiscard);
	deletePlace.setOnClickListener(new View.OnClickListener() { 

	    @Override
	    public void onClick(View view) {
		try {	

		    //Ask the user if they want to delete the patient
		    //AlertDialog alertDialog =  new AlertDialog.Builder(myTools.getContext()).create();
		    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
		    alertDialog.setTitle(R.string.places_activity_discard_title);
		    alertDialog.setMessage(Tools.getStringResource(R.string.places_activity_discard_message));
		    alertDialog.setButton(Tools.getStringResource(R.string.places_activity_discard_yes), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {			    
			    datasource.open();
			    datasource.deletePlace((long) currentPlaceID.intParcelable);
			   
			    Intent k = new Intent(PlacesActivityShow.this, PlacesActivity.class);
			    startActivity(k); 
			    
			    datasource.close();
			    
			    Tools.showToastMessage(Tools.getContext(), Tools.getStringResource(R.string.places_activity_disard_confirmation_message));
			}
		    });

		    alertDialog.setButton2(Tools.getStringResource(R.string.places_activity_discard_no), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		    });

		    alertDialog.show();

		} catch(Exception e) {
		    Log.v(LOGGER_TAG, "Error deleting place: " + e.toString());
		}	        		
	    }
	}); 
    }
}
