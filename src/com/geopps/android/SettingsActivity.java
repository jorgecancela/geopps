package com.geopps.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.geopps.android.db.SettingsDataSource;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.Settings;
import com.geopps.android.utils.Tools;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class SettingsActivity extends Activity {
    private SettingsDataSource datasource;
    private Button saveButton;
    private RadioButton spanishRadio, englishRadio;
    private LinearLayout gCalendarSyncPanel;
    private CheckBox cbGCalendar;
    private Settings currentSet;
    private String language;
    private Boolean booleanGCalendar = false;
    private String LOGGER_TAG = "SettingsActivity";
    
    @Override
    protected void onPause() {
	try {

	    datasource.close();

	} catch(Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception: " + e.toString());
	}
	
	super.onPause();
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	// Read the last configuration stored in the database and setup the
	// configuration
	try {
	    Tools.setSettings(this.getApplicationContext());
	    Tools.setContext(this.getApplicationContext());
	} catch(Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    LOGGER_TAG, "Exception setting up context: " + e.toString());
	}

	setContentView(R.layout.settings_main);

	spanishRadio = (RadioButton) findViewById(R.id.radioLanguageSpanish);
	englishRadio = (RadioButton) findViewById(R.id.radioLanguageEnglish);

	cbGCalendar = (CheckBox) findViewById(R.id.checkBoxSyncGCalendar);
	gCalendarSyncPanel =(LinearLayout)this.findViewById(R.id.linearLayoutGCalendarSync);
	
	if ((Build.VERSION.SDK_INT < 11)||(!Constants.GCALENDAR_SYNC_FLAG)){
	    gCalendarSyncPanel.setVisibility(Button.GONE);
	}
	   
	showConfig();

	saveButton = (Button) findViewById(R.id.buttonSaveSettings);
	saveButton.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View view) {
		try {

		    // Read Sync Google Calendar Check Box
		    if(cbGCalendar.isChecked()){
			booleanGCalendar = true;
		    }else{
			booleanGCalendar = false;
		    }

		    // Read language RadioButton
		    if(spanishRadio.isChecked()) {
			language = Constants.LANGUAGE_ES;
		    }

		    if(englishRadio.isChecked()) {
			language = Constants.LANGUAGE_EN;
		    }

		    storeSettings(); 

		} catch (Exception e) {
		    Log.e(LOGGER_TAG, "Error: " + e.toString());
		}
	    }
	});
    }


    private void showConfig() {

	try {

	    datasource = new SettingsDataSource(this.getApplicationContext());
	    datasource.open();
	    currentSet = datasource.getLastSettings();

	} catch(Exception e) {
	    Log.e(LOGGER_TAG, "Error opening Settings datasource" + e.toString());
	}	

	cbGCalendar.setChecked(currentSet.getGCalendarBoolean());

	if(currentSet.getLanguage() == null) {
	    
	    englishRadio.setChecked(true);			 	 
	
	} else {
	    
	    if(currentSet.getLanguage().equals(Constants.LANGUAGE_ES)) {
		spanishRadio.setChecked(true);
	    }

	    if(currentSet.getLanguage().equals(Constants.LANGUAGE_DEFAULT)) {
		englishRadio.setChecked(true);
	    }
	
	}

	datasource.close();
    }

    private void storeSettings() {
	try {

	    datasource = new SettingsDataSource(this.getApplicationContext());
	    datasource.open();  
	    datasource.createSettings("", "", "", "", language, "", "", "", "", Boolean.toString(booleanGCalendar), "","");
	    datasource.close();

	} catch(Exception e) {
	    Log.e(LOGGER_TAG, "Error saving a new Settings: " + e.toString());
	}

	try {

	    Intent k = new Intent(SettingsActivity.this, AlertsActivity.class);
	    startActivity(k);  

	} catch(Exception e) {
	    Log.e(LOGGER_TAG, "Error trying to start main screen: " + e.toString());
	}
    }
}
