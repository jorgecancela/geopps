package com.geopps.android.db;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.geopps.android.utils.Alert;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.Tools;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/

public class AlertsDataSource {
    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    private String[] allColumns = { 
	    MySQLiteHelper.COLUMN_A_ID,
	    MySQLiteHelper.COLUMN_A_START_DATE, 
	    MySQLiteHelper.COLUMN_A_END_DATE, 
	    MySQLiteHelper.COLUMN_A_TIMEZONE, 
	    MySQLiteHelper.COLUMN_A_COMMENT,
	    MySQLiteHelper.COLUMN_A_RINGTONE,
	    MySQLiteHelper.COLUMN_A_FLAG_ACTIVE,
	    MySQLiteHelper.COLUMN_A_TYPE,
	    MySQLiteHelper.COLUMN_A_PLACE_ID
    };

    public AlertsDataSource(Context context) {
	dbHelper = new MySQLiteHelper(context);
    }

    public void close() {
	dbHelper.close();
    }

    public Alert createAlert(String startDate, String endDate, String timezone, String comment, String ringtone, String flagActive, int type, int placeId){
	Tools.activePendingAlerts();

	ContentValues values = new ContentValues();
	values.put(MySQLiteHelper.COLUMN_A_START_DATE, startDate);
	values.put(MySQLiteHelper.COLUMN_A_END_DATE, endDate);
	values.put(MySQLiteHelper.COLUMN_A_TIMEZONE, timezone);
	values.put(MySQLiteHelper.COLUMN_A_COMMENT, comment);
	values.put(MySQLiteHelper.COLUMN_A_RINGTONE, ringtone);
	values.put(MySQLiteHelper.COLUMN_A_FLAG_ACTIVE, flagActive);
	values.put(MySQLiteHelper.COLUMN_A_TYPE, type);
	values.put(MySQLiteHelper.COLUMN_A_PLACE_ID, placeId);

	this.open();

	long insertId = database.insert(MySQLiteHelper.TABLE_ALERTS, null, values);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_ALERTS, allColumns, MySQLiteHelper.COLUMN_A_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	Alert newAlert = cursorToAlert(cursor);
	cursor.close();
	
	// Creates related tables with this alert e.g. Google Calendar
	newAlert.createRelatedTables();

	return newAlert;
    }

    public Alert updateAlert(Integer aid, String startDate, String endDate, String timezone, String comment, String ringtone, String flagActive, int type, int placeId) {
	Tools.activePendingAlerts();

	ContentValues values = new ContentValues();
	values.put(MySQLiteHelper.COLUMN_A_START_DATE, startDate);
	values.put(MySQLiteHelper.COLUMN_A_END_DATE, endDate);
	values.put(MySQLiteHelper.COLUMN_A_TIMEZONE, timezone);
	values.put(MySQLiteHelper.COLUMN_A_COMMENT, comment);
	values.put(MySQLiteHelper.COLUMN_A_RINGTONE, ringtone);
	values.put(MySQLiteHelper.COLUMN_A_FLAG_ACTIVE, flagActive);
	values.put(MySQLiteHelper.COLUMN_A_TYPE, type);
	values.put(MySQLiteHelper.COLUMN_A_PLACE_ID, placeId);

	this.open();

	long insertId = database.update(MySQLiteHelper.TABLE_ALERTS, values, MySQLiteHelper.COLUMN_A_ID + " = " + aid, null);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_ALERTS, allColumns, MySQLiteHelper.COLUMN_A_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	Alert newAlert = cursorToAlert(cursor);
	newAlert.createRelatedTables();
	cursor.close();

	return newAlert;
    }

    private Alert cursorToAlert(Cursor cursor) {
	Alert alert = new Alert();

	alert.setId(cursor.getInt(0));
	alert.setStartDate(cursor.getString(1));
	alert.setEndDate(cursor.getString(2));
	alert.setTimezone(cursor.getString(3));
	alert.setComment(cursor.getString(4));
	alert.setRingtone(cursor.getString(5));
	alert.setFlagActive(cursor.getString(6));
	alert.setType(cursor.getInt(7));
	alert.setPID(cursor.getInt(8));

	return alert;
    }

    public void deleteAlert(Long id) {
	database.delete(MySQLiteHelper.TABLE_ALERTS, MySQLiteHelper.COLUMN_A_ID + " = " + id, null);
    }

    public List<Alert> getAllAlerts() {
	List<Alert> alerts = new ArrayList<Alert>();

	Cursor cursor = database.query(MySQLiteHelper.TABLE_ALERTS, allColumns, null, null, null, null, "LOWER(" + MySQLiteHelper.COLUMN_A_FLAG_ACTIVE + ") DESC");

	cursor.moveToFirst();
	while (!cursor.isAfterLast()) {
	    Alert alert = cursorToAlert(cursor);
	    alerts.add(alert);
	    cursor.moveToNext();
	}
	// Make sure to close the cursor
	cursor.close();
	database.close();
	return alerts;
    }

    public Alert getAlertById(long alertID) {
	Alert newAlert = null;

	try {
	    //  Cursor cursor = database.query(MySQLiteHelper.TABLE_ALERTS,   MySQLiteHelper.COLUMN_A_ID + " = " + alertID, null , null, null, null);
	    Cursor cursor = database.query(MySQLiteHelper.TABLE_ALERTS, allColumns, MySQLiteHelper.COLUMN_A_ID + " = " + (long) alertID, null, null, null, null);

	    cursor.moveToFirst();
	    newAlert = cursorToAlert(cursor);
	    cursor.close();

	} catch (Exception e) {
	    Log.v("AlertsDataSource", "getAlertById exception: " + e.toString());
	}
	return newAlert;	
    }

    public void open() throws SQLException {
	database = dbHelper.getWritableDatabase();
    }

    public List<Alert> getAllActiveAlerts() {
	List<Alert> alerts = new ArrayList<Alert>();

	Cursor cursor = database.query(MySQLiteHelper.TABLE_ALERTS, 
		allColumns, 
		MySQLiteHelper.COLUMN_A_FLAG_ACTIVE + "=? ", new String[] {"true"}, null, null, null);

	cursor.moveToFirst();
	while (!cursor.isAfterLast()) {
	    Alert alert = cursorToAlert(cursor);
	    alerts.add(alert);
	    cursor.moveToNext();
	}

	// Make sure to close the cursor
	cursor.close();

	return alerts;
    }

    public Alert setActive(Long pid) {	
	ContentValues values = new ContentValues();

	values.put(MySQLiteHelper.COLUMN_A_FLAG_ACTIVE , Constants.BOOLEAN_TRUE);

	long insertId = database.update(MySQLiteHelper.TABLE_ALERTS, values, MySQLiteHelper.COLUMN_A_ID + " = " + pid, null);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_ALERTS, allColumns, MySQLiteHelper.COLUMN_A_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	Alert newAlert = cursorToAlert(cursor);
	cursor.close();

	return newAlert;
    }

    public Alert setActiveFalse(Long pid) {	
	ContentValues values = new ContentValues();

	values.put(MySQLiteHelper.COLUMN_A_FLAG_ACTIVE , Constants.BOOLEAN_FALSE);

	long insertId = database.update(MySQLiteHelper.TABLE_ALERTS, values, MySQLiteHelper.COLUMN_A_ID + " = " + pid, null);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_ALERTS, allColumns, MySQLiteHelper.COLUMN_A_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	Alert newAlert = cursorToAlert(cursor);
	cursor.close();

	return newAlert;
    }

}

