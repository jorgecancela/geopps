package com.geopps.android.db;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.geopps.android.utils.Constants;
import com.geopps.android.utils.GCalendar;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class GCalendarDataSource {
    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    private String[] allColumns = { 
	    MySQLiteHelper.COLUMN_GC_ID,
	    MySQLiteHelper.COLUMN_GC_DATE, 
	    MySQLiteHelper.COLUMN_GC_LOCAL_ALERT_ID,
	    MySQLiteHelper.COLUMN_GC_GCALENDAR_ID,
	    MySQLiteHelper.COLUMN_GC_FLAG_ACTIVE
    };

    public GCalendarDataSource(Context context) {
	dbHelper = new MySQLiteHelper(context);
    }

    public void close() {
	dbHelper.close();
    }

    public GCalendar createGCalendar(String date, long localAlertID, String gCalendarID, String flagActive){
	ContentValues values = new ContentValues();

	values.put(MySQLiteHelper.COLUMN_GC_DATE, date);
	values.put(MySQLiteHelper.COLUMN_GC_LOCAL_ALERT_ID, localAlertID);
	values.put(MySQLiteHelper.COLUMN_GC_GCALENDAR_ID, gCalendarID);
	values.put(MySQLiteHelper.COLUMN_GC_FLAG_ACTIVE, flagActive);

	this.open();

	long insertId = database.insert(MySQLiteHelper.TABLE_GCALENDAR, null, values);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, allColumns, MySQLiteHelper.COLUMN_GC_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	GCalendar newGCalendar = cursorToGCalendar(cursor);
	cursor.close();
	Log.e("Ttrack"," " + newGCalendar.getDateString() + " " + newGCalendar.getLocalAlertId() + " "+ gCalendarID + " " + flagActive + " " );
	return newGCalendar;
    }

    public GCalendar updateGCalendar(Integer gcId, String date, Integer localAlertID, String gCalendarID, String flagActive) {
	ContentValues values = new ContentValues();
	
	values.put(MySQLiteHelper.COLUMN_GC_DATE, date);
	values.put(MySQLiteHelper.COLUMN_GC_LOCAL_ALERT_ID, localAlertID);
	values.put(MySQLiteHelper.COLUMN_GC_GCALENDAR_ID, gCalendarID);
	values.put(MySQLiteHelper.COLUMN_GC_FLAG_ACTIVE, flagActive);

	this.open();

	long insertId = database.update(MySQLiteHelper.TABLE_GCALENDAR, values, MySQLiteHelper.COLUMN_GC_ID + " = " + gcId, null);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, allColumns, MySQLiteHelper.COLUMN_GC_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	GCalendar newGCalendar = cursorToGCalendar(cursor);
	cursor.close();

	return newGCalendar;
    }

    private GCalendar cursorToGCalendar(Cursor cursor) {
	GCalendar newGCalendar = new GCalendar();

	newGCalendar.setId(cursor.getInt(0));
	newGCalendar.setDate(cursor.getString(1));
	newGCalendar.setLocalAlertId(cursor.getInt(2));
	newGCalendar.setGCalendarId(cursor.getString(3));
	newGCalendar.setFlagActive(cursor.getString(4));

	return newGCalendar;
    }

    public void deleteAlert(Long gcId) {
	database.delete(MySQLiteHelper.TABLE_GCALENDAR, MySQLiteHelper.COLUMN_GC_ID + " = " + gcId, null);
    }

    public void open() throws SQLException {
	database = dbHelper.getWritableDatabase();
    }

    public List<GCalendar> getAllActiveGCalendar() {
	List<GCalendar> gCalendars = new ArrayList<GCalendar>();

	Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, 
		allColumns, 
		MySQLiteHelper.COLUMN_GC_FLAG_ACTIVE + "=? ", new String[] {"true"}, null, null, null);

	cursor.moveToFirst();

	while (!cursor.isAfterLast()) {
	    GCalendar gCalendar = cursorToGCalendar(cursor);
	    gCalendars.add(gCalendar);
	    cursor.moveToNext();
	}

	// Make sure to close the cursor
	cursor.close();
	database.close();
	return gCalendars;
    }

    public List<GCalendar> getAllNonActiveGCalendar() {
	List<GCalendar> gCalendars = new ArrayList<GCalendar>();

	Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, 
		allColumns, 
		MySQLiteHelper.COLUMN_GC_FLAG_ACTIVE + "=? ", new String[] {"false"}, null, null, null);

	cursor.moveToFirst();

	while (!cursor.isAfterLast()) {
	    GCalendar gCalendar = cursorToGCalendar(cursor);
	    gCalendars.add(gCalendar);
	    cursor.moveToNext();
	}

	// Make sure to close the cursor
	cursor.close();
	database.close();
	return gCalendars;
    }

    /**
    public GCalendar getGCalendarByAlertId(int gcId) {

	GCalendar newGCalendar = new GCalendar();

	try {

	    Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, allColumns, MySQLiteHelper.COLUMN_GC_LOCAL_ALERT_ID + " = " + gcId, null, null, null, null);

	    cursor.moveToFirst();
	    newGCalendar = cursorToGCalendar(cursor);
	    cursor.close();

	    database.close();

	} catch (Exception e) {
	    Log.v("GCalendarDataSource", "getGCalendar exception: " + e.toString());
	}
	return newGCalendar;	
    }
     */

    /**
    public GCalendar getGCalendarActiveByAlertId(long id) {

	GCalendar newGCalendar = new GCalendar();

	try {

	    Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, allColumns, MySQLiteHelper.COLUMN_GC_LOCAL_ALERT_ID + " = " + id +" AND " + MySQLiteHelper.COLUMN_GC_FLAG_ACTIVE + " = ?", new String[] {"true"}, null, null, null);

	    cursor.moveToFirst();
	    newGCalendar = cursorToGCalendar(cursor);
	    cursor.close();

	    database.close();

	} catch (Exception e) {
	    Log.v("GCalendarDataSource", "getGCalendar exception: " + e.toString());
	}

	return newGCalendar;	
    }

     */
    /**
    public void setAllActiveFalse() {	

	Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, 
		allColumns, 
		MySQLiteHelper.COLUMN_A_FLAG_ACTIVE + "=? ", new String[] {"true"}, null, null, null);

	cursor.moveToFirst();

	ContentValues values = new ContentValues();
	values.put(MySQLiteHelper.COLUMN_GC_FLAG_ACTIVE , Constants.BOOLEAN_FALSE);

	while (!cursor.isAfterLast()) {
	    GCalendar gCalendar = cursorToGCalendar(cursor);
	    database.update(MySQLiteHelper.TABLE_GCALENDAR, values, MySQLiteHelper.COLUMN_GC_ID + " = " + gCalendar.getId(), null);
	    cursor.moveToNext();
	}

	// Make sure to close the cursor
	cursor.close();
	database.close();
    }
     */

    /**
    public void updateGCalendarFlags(List<Alert> activeAlerts) {
	setAllActiveFalse();

	open();

	for(Alert a:activeAlerts){
	    GCalendar currentGcal = getGCalendarByAlertId((int) a.getId());

	    if(currentGcal.getId() <= 0){
		Log.v("Track", "entra aqio" );
		createGCalendar(Tools.CalendarToStringToDb(Calendar.getInstance()), (int) a.getId(), "", "true");

	    }else{
		Log.v("Track", " o entra aqio" );
		updateGCalendar((int) currentGcal.getId(), Tools.CalendarToStringToDb(Calendar.getInstance()), (int) a.getId(), "", "true");

	    }	    
	}

	close();
	Log.v("Track", "3");

    }*/

    public GCalendar updateGCalendarId(String gCalendarId, Long gcId) {	
	this.open();
	Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, allColumns, MySQLiteHelper.COLUMN_GC_LOCAL_ALERT_ID + " = " + gcId, null, null, null, null);

	cursor.moveToFirst();
	GCalendar newGCalendar = cursorToGCalendar(cursor);
	cursor.close();

	Log.e("Ttrack","_____1a" + gCalendarId + " " + gcId);
	ContentValues values = new ContentValues();	
	values.put(MySQLiteHelper.COLUMN_GC_GCALENDAR_ID , gCalendarId);
	Log.e("Ttrack","_____1b");
	long insertId = database.update(MySQLiteHelper.TABLE_GCALENDAR, values, MySQLiteHelper.COLUMN_GC_ID + " = " + newGCalendar.getId(), null);
	Log.e("Ttrack","_____1c");
	cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, allColumns, MySQLiteHelper.COLUMN_GC_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	newGCalendar = cursorToGCalendar(cursor);
	cursor.close();
	Log.e("Ttrack","_____1d");
	return newGCalendar;
    }

    public GCalendar setActive(Long gcId) {	
	ContentValues values = new ContentValues();

	values.put(MySQLiteHelper.COLUMN_GC_FLAG_ACTIVE , Constants.BOOLEAN_TRUE);

	// Create the event on Google calendar and update this field


	long insertId = database.update(MySQLiteHelper.TABLE_GCALENDAR, values, MySQLiteHelper.COLUMN_GC_LOCAL_ALERT_ID + " = " + gcId, null);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, allColumns, MySQLiteHelper.COLUMN_GC_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	GCalendar newGCalendar = cursorToGCalendar(cursor);
	cursor.close();

	return newGCalendar;
    }

    public GCalendar setActiveFalse(Long gcId) {	
	ContentValues values = new ContentValues();

	values.put(MySQLiteHelper.COLUMN_GC_FLAG_ACTIVE , Constants.BOOLEAN_FALSE);
	values.put(MySQLiteHelper.COLUMN_GC_GCALENDAR_ID , "");

	long insertId = database.update(MySQLiteHelper.TABLE_GCALENDAR, values, MySQLiteHelper.COLUMN_GC_LOCAL_ALERT_ID + " = " + gcId, null);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_GCALENDAR, allColumns, MySQLiteHelper.COLUMN_GC_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	GCalendar newGCalendar = cursorToGCalendar(cursor);
	cursor.close();

	return newGCalendar;
    }
}

