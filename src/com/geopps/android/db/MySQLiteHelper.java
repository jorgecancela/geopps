package com.geopps.android.db;

import java.io.File;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class MySQLiteHelper extends SQLiteOpenHelper {
    public static final String TABLE_PLACES = "places";
    public static final String COLUMN_P_ID = "_id";
    public static final String COLUMN_P_NAME = "name";
    public static final String COLUMN_P_ADDRESS = "address";
    public static final String COLUMN_P_LAT = "lat";
    public static final String COLUMN_P_LON = "lon";
    public static final String COLUMN_P_RADIUS = "radius";

    public static final String TABLE_SETTINGS = "settings";
    public static final String COLUMN_S_ID = "_id";
    public static final String COLUMN_S_RESIDENCE = "residence";
    public static final String COLUMN_S_EMAIL = "email";
    public static final String COLUMN_S_USERNAME = "username";
    public static final String COLUMN_S_PASSWORD = "password";
    public static final String COLUMN_S_LANGUAGE = "language";
    public static final String COLUMN_S_TWITTER = "twitter";
    public static final String COLUMN_S_FACEBOOK = "facebook";
    public static final String COLUMN_S_FOURSQUARE = "foursquare";
    public static final String COLUMN_S_PINTEREST = "pinterest";
    public static final String COLUMN_S_GCALENDAR = "gcalendar";
    public static final String COLUMN_S_GCALENDAR_GCAL_ID = "gcalendarID";
    public static final String COLUMN_S_DATESTORED = "dateStored";

    public static final String TABLE_ALERTS = "alerts";
    public static final String COLUMN_A_ID = "_id";
    public static final String COLUMN_A_START_DATE = "start_date";
    public static final String COLUMN_A_END_DATE = "end_date";
    public static final String COLUMN_A_TIMEZONE = "timezone";
    public static final String COLUMN_A_COMMENT = "comment";
    public static final String COLUMN_A_RINGTONE = "ringtone";
    public static final String COLUMN_A_FLAG_ACTIVE = "active";
    public static final String COLUMN_A_TYPE = "type";   
    public static final String COLUMN_A_PLACE_ID = "place_id";
    public static final String COLUMN_A_PLACE = "FOREIGN KEY(place_id) REFERENCES place(_id)";
    
    public static final String TABLE_GCALENDAR = "gcalendar";
    public static final String COLUMN_GC_ID = "_id";
    public static final String COLUMN_GC_DATE = "date";
    public static final String COLUMN_GC_LOCAL_ALERT_ID = "event_local_alert_id";
    public static final String COLUMN_GC_GCALENDAR_ID = "event_gcalendar_id";
    public static final String COLUMN_GC_FLAG_ACTIVE = "active";
        
    public static final String DATABASE_NAME = "geopps.db";
    public static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE_PLACES = "create table "
	+ TABLE_PLACES + "( " 
	+ COLUMN_P_ID + " integer primary key autoincrement, " 
	+ COLUMN_P_NAME + " text not null," 
	+ COLUMN_P_ADDRESS + " text," 
	+ COLUMN_P_LAT + " numeric," 
	+ COLUMN_P_LON + " numeric,"  
	+ COLUMN_P_RADIUS + " numeric" 
	+ ");";
    
    private static final String DATABASE_CREATE_SETTINGS = "create table "
	+ TABLE_SETTINGS + "( " 
	+ COLUMN_S_ID + " integer primary key autoincrement, " 
	+ COLUMN_S_RESIDENCE + " text," 
	+ COLUMN_S_EMAIL + " text," 
	+ COLUMN_S_USERNAME + " text," 
	+ COLUMN_S_PASSWORD + " text," 
	+ COLUMN_S_LANGUAGE + " text,"
	+ COLUMN_S_TWITTER + " text," 
	+ COLUMN_S_FACEBOOK + " text,"
	+ COLUMN_S_FOURSQUARE + " text," 
	+ COLUMN_S_PINTEREST + " text," 
	+ COLUMN_S_GCALENDAR + " text," 
	+ COLUMN_S_GCALENDAR_GCAL_ID + " text," 
	+ COLUMN_S_DATESTORED + " text"
	+ ");";
        
    private static final String DATABASE_CREATE_ALERTS = "create table "
	+ TABLE_ALERTS + "( " 
	+ COLUMN_A_ID + " integer primary key autoincrement, " 
	+ COLUMN_A_START_DATE + " text," 
	+ COLUMN_A_END_DATE + " text," 
	+ COLUMN_A_TIMEZONE + " text," 
	+ COLUMN_A_COMMENT + " text," 
	+ COLUMN_A_RINGTONE + " text," 
	+ COLUMN_A_FLAG_ACTIVE + " text," 
	+ COLUMN_A_TYPE + " integer," 
	+ COLUMN_A_PLACE_ID + " integer," 
	+ COLUMN_A_PLACE
	+ ");";
    
    private static final String DATABASE_CREATE_GCALENDAR = "create table "
	+ TABLE_GCALENDAR + "( " 
	+ COLUMN_GC_ID + " integer primary key autoincrement, " 
	+ COLUMN_GC_DATE + " text," 
	+ COLUMN_GC_LOCAL_ALERT_ID + " int," 
	+ COLUMN_GC_GCALENDAR_ID + " text," 
	+ COLUMN_GC_FLAG_ACTIVE + " text" 
	+ ");";
    
    /** Constructor */
    public MySQLiteHelper(Context context) {
	super(context, DATABASE_NAME, null, DATABASE_VERSION);	
    }	

    /**  Checks if a database already exists   */
    public boolean databaseExist() {
	File dbFile = new File(DATABASE_NAME);
	return dbFile.exists();
    }

    @Override
    public void onCreate(SQLiteDatabase database) {  
	 Log.v("tracker "," creando database if null ");
	 
	try {
	    database.execSQL(DATABASE_CREATE_PLACES);	
	} catch(Exception e) {
	    Log.v("MySQLiteHelper", "Error creating PLACES database:" + e.toString());
	}

	try {
	    database.execSQL(DATABASE_CREATE_SETTINGS);
	} catch(Exception e) {
	    Log.v("MySQLiteHelper", "Error creating SETTINGS database:" + e.toString());
	}
	
	try {
	    database.execSQL(DATABASE_CREATE_ALERTS);
	} catch(Exception e) {
	    Log.v("MySQLiteHelper", "Error creating ALERTS database:" + e.toString());
	}

	try {
	    database.execSQL(DATABASE_CREATE_GCALENDAR);
	} catch(Exception e) {
	    Log.v("MySQLiteHelper", "Error creating GCALENDAR database:" + e.toString());
	}
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

