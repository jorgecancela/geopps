package com.geopps.android.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.geopps.android.utils.Place;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class PlacesDataSource {
    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    private String[] allColumns = { 
	    MySQLiteHelper.COLUMN_P_ID,
	    MySQLiteHelper.COLUMN_P_NAME, 
	    MySQLiteHelper.COLUMN_P_ADDRESS,
	    MySQLiteHelper.COLUMN_P_LAT,
	    MySQLiteHelper.COLUMN_P_LON,
	    MySQLiteHelper.COLUMN_P_RADIUS,
    };

    public PlacesDataSource(Context context) {
	dbHelper = new MySQLiteHelper(context);
    }

    public void close() {
	dbHelper.close();
    }

    public Place createPlace(String name, String address, long lon, long lat, long radius){

	ContentValues values = new ContentValues();
	values.put(MySQLiteHelper.COLUMN_P_NAME, name);
	values.put(MySQLiteHelper.COLUMN_P_ADDRESS, address);
	values.put(MySQLiteHelper.COLUMN_P_LAT, lon);
	values.put(MySQLiteHelper.COLUMN_P_LON, lat);
	values.put(MySQLiteHelper.COLUMN_P_RADIUS, radius);

	long insertId = database.insert(MySQLiteHelper.TABLE_PLACES, null, values);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_PLACES, allColumns, MySQLiteHelper.COLUMN_P_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	Place newPlace = cursorToPlace(cursor);
	cursor.close();

	return newPlace;
    }

    private Place cursorToPlace(Cursor cursor) {
	Place place = new Place();

	place.setId(cursor.getInt(0));
	place.setName(cursor.getString(1));
	place.setAddress(cursor.getString(2));
	place.setLat(cursor.getLong(3));
	place.setLon(cursor.getLong(4));
	place.setRadius(cursor.getLong(5));

	return place;
    }

    public void deletePlace(Long pid) {
	database.delete(MySQLiteHelper.TABLE_PLACES, MySQLiteHelper.COLUMN_P_ID + " = " + pid, null);
    }

    public List<Place> getAllPlaces() {
	List<Place> places = new ArrayList<Place>();

	Cursor cursor = database.query(MySQLiteHelper.TABLE_PLACES, allColumns, null, null, null, null, "LOWER(" + MySQLiteHelper.COLUMN_P_NAME + ") ASC");

	cursor.moveToFirst();
	while (!cursor.isAfterLast()) {
	    Place place = cursorToPlace(cursor);
	    places.add(place);
	    cursor.moveToNext();
	}
	// Make sure to close the cursor
	cursor.close();
	database.close();
	return places;
    }

    public Place getPlaceById(int placeID) {

	Place newPlace = new Place();

	try {
	    Cursor cursor = database.query(MySQLiteHelper.TABLE_PLACES, allColumns, MySQLiteHelper.COLUMN_P_ID + " = " + (long) placeID, null, null, null, null);

	    cursor.moveToFirst();
	    newPlace = cursorToPlace(cursor);
	    cursor.close();

	    database.close();

	} catch (Exception e) {
	    Log.v("PlacesDataSource", "getPlaceById exception: " + e.toString());
	}
	return newPlace;	
    }

    public void open() throws SQLException {
	database = dbHelper.getWritableDatabase();
    }

    public Place searchPlace(String name) {
	Place newPlace = new Place();

	try {
	    Cursor cursor = database.query(MySQLiteHelper.TABLE_PLACES,
		    allColumns, 
		    MySQLiteHelper.COLUMN_P_NAME + "=? ", new String[] {name}, null, null, null, null);

	    cursor.moveToFirst();
	    newPlace = cursorToPlace(cursor);
	    cursor.close();

	    database.close();

	} catch (Exception e) {
	    Log.v("PlaceDataSource", "searchPlace exception: " + e.toString());
	    return null;
	}
	return newPlace;	
    }
}

