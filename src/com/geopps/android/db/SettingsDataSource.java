package com.geopps.android.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.geopps.android.R;
import com.geopps.android.utils.Settings;
import com.geopps.android.utils.Tools;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class SettingsDataSource {
    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    private String[] allColumns = { 
	    MySQLiteHelper.COLUMN_S_ID,
	    MySQLiteHelper.COLUMN_S_RESIDENCE, 
	    MySQLiteHelper.COLUMN_S_EMAIL,
	    MySQLiteHelper.COLUMN_S_USERNAME,
	    MySQLiteHelper.COLUMN_S_PASSWORD,
	    MySQLiteHelper.COLUMN_S_LANGUAGE,
	    MySQLiteHelper.COLUMN_S_TWITTER,
	    MySQLiteHelper.COLUMN_S_FACEBOOK,
	    MySQLiteHelper.COLUMN_S_FOURSQUARE,
	    MySQLiteHelper.COLUMN_S_PINTEREST,
	    MySQLiteHelper.COLUMN_S_GCALENDAR,
	    MySQLiteHelper.COLUMN_S_GCALENDAR_GCAL_ID,
	    MySQLiteHelper.COLUMN_S_DATESTORED
    };
        
    public SettingsDataSource(Context context) {
	
	try {
	    dbHelper = new MySQLiteHelper(context);
	} catch (Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    "SettingsDataSource", "Exception SettingsDataSource: " + e.toString());
	}
    }

    public void close() {
	dbHelper.close();
    }
    
    public void open() throws SQLException {
	database = dbHelper.getWritableDatabase();
    }
    
    public Settings createSettings(String residence, String email, String username, String password,
	                         String language, String twitter, String facebook, String foursquare,
	                         String pinterest, String gcalendar, String gcalendarId, String date){

	ContentValues values = new ContentValues();
	values.put(MySQLiteHelper.COLUMN_S_RESIDENCE, residence);
	values.put(MySQLiteHelper.COLUMN_S_EMAIL, email);
	values.put(MySQLiteHelper.COLUMN_S_USERNAME, username);
	values.put(MySQLiteHelper.COLUMN_S_PASSWORD, password);
	values.put(MySQLiteHelper.COLUMN_S_LANGUAGE, language);
	values.put(MySQLiteHelper.COLUMN_S_TWITTER, twitter);
	values.put(MySQLiteHelper.COLUMN_S_FACEBOOK, facebook);
	values.put(MySQLiteHelper.COLUMN_S_FOURSQUARE, foursquare);
	values.put(MySQLiteHelper.COLUMN_S_PINTEREST, pinterest);
	values.put(MySQLiteHelper.COLUMN_S_GCALENDAR, gcalendar);
	values.put(MySQLiteHelper.COLUMN_S_GCALENDAR_GCAL_ID, gcalendarId);
	values.put(MySQLiteHelper.COLUMN_S_DATESTORED, date);
	
	long insertId = database.insert(MySQLiteHelper.TABLE_SETTINGS, null, values);

	Cursor cursor = database.query(MySQLiteHelper.TABLE_SETTINGS, allColumns, MySQLiteHelper.COLUMN_S_ID + " = " + insertId, null, null, null, null);
	cursor.moveToFirst();
	Settings newSetting = cursorToSettings(cursor);
	cursor.close();

	return newSetting;
    }

    private Settings cursorToSettings(Cursor cursor) {
	Settings setting = new Settings();

	setting.setId(cursor.getInt(0));
	setting.setResidence(cursor.getString(1));
	setting.setEmail(cursor.getString(2));
	setting.setUsername(cursor.getString(3));
	setting.setPassword(cursor.getString(4));
	setting.setLanguage(cursor.getString(5));
	setting.setTwitter(cursor.getString(6));
	setting.setFacebook(cursor.getString(7));
	setting.setFoursquare(cursor.getString(8));
	setting.setPinterest(cursor.getString(9));
	setting.setGCalendar(cursor.getString(10));
	setting.setGCalendarId(cursor.getString(11));
	setting.setDate(cursor.getString(12));
     
	return setting;
    }
    
    public void deleteSettings(Settings setting) {
	long id = setting.getId();
	database.delete(MySQLiteHelper.TABLE_SETTINGS, MySQLiteHelper.COLUMN_S_ID + " = " + id, null);
    }

    public List<Settings> getAllSettings() {
	List<Settings> settings = new ArrayList<Settings>();

	Cursor cursor = database.query(MySQLiteHelper.TABLE_SETTINGS, allColumns, null, null, null, null, MySQLiteHelper.COLUMN_S_DATESTORED + "ASC");

	cursor.moveToFirst();
	while (!cursor.isAfterLast()) {
	    Settings setting = cursorToSettings(cursor);
	    settings.add(setting);
	    cursor.moveToNext();
	}
	
	// Make sure to close the cursor
	cursor.close();
	database.close();
	return settings;
    }
    
    public Settings getLastSettings() {
	Cursor cursor;
	Settings newSet = new Settings();

	try {

	    cursor = database.query(MySQLiteHelper.TABLE_SETTINGS, allColumns, null, null, null, null, MySQLiteHelper.COLUMN_S_ID + " DESC");

	    if(cursor.getCount() == 0) {
		
		cursor.close();
		return newSet;  
		
	    } else {
		
		cursor.moveToFirst();
		newSet = cursorToSettings(cursor);
		cursor.close();
		return newSet;
		
	    }
	    
	} catch(Exception e) {
	    Tools.showToastMessageAndLog(Tools.getContext(), Tools.getStringResource(R.string.unknown_error),
		    "SettingsDataSource", "Exception getLastSettings: " + e.toString());
	    return newSet;
	}

    }

}

