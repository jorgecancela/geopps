package com.geopps.android.gcalendar;

import com.geopps.android.utils.Constants;
import com.google.api.services.calendar.model.CalendarList;
import java.io.IOException;

/**
 * Asynchronously remove a local event in the Geopps calendar
 * 
 * @author jcancela
 */
class AsyncRemoveLocalEventInGoogleCalendar extends CalendarAsyncTask {
    private String gCalendarID;

    AsyncRemoveLocalEventInGoogleCalendar(CalendarSampleActivity calendarSample) {
	super(calendarSample);
    }

    @Override
    protected void doInBackground() throws IOException {
	
	// Get all the calendars and get the ID of the Geopps calendar
	CalendarList feed = client.calendarList().list().setFields(CalendarInfo.FEED_FIELDS).execute();
	model.reset(feed.getItems());
	CalendarInfo[] sortedArray = model.toSortedArray();

	String geoppsCalendarId = null;

	for (int i = 0; i < sortedArray.length; i++) {
	    if (sortedArray[i].summary.toLowerCase().equals(Constants.GOOGLE_CALENDAR_NAME_LOWERCASE)) {
		geoppsCalendarId = sortedArray[i].id;
	    }
	}
	
	client.events().delete(geoppsCalendarId, gCalendarID).execute();
	
	/**
	Events ee = client.events().list(geoppsCalendarId).execute();

	List<Event> lee = ee.getItems();

	for (int i = 0; i < lee.size(); i++) {
	   
	    if(lee.get(i).getId().compareTo(gCalendarID)){
		client.events().delete(geoppsCalendarId, gCalendarID).execute();
	    }

	}**/
    }
    
    public void setIdToRemove(String gCalendarID){
	this.gCalendarID = gCalendarID;
    }

    static void run(CalendarSampleActivity calendarSample) {
	new AsyncRemoveLocalEventInGoogleCalendar(calendarSample).execute();
    }
}
