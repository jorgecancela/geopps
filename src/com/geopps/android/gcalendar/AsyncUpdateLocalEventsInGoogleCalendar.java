package com.geopps.android.gcalendar;

import android.util.Log;

import com.geopps.android.db.AlertsDataSource;
import com.geopps.android.db.GCalendarDataSource;
import com.geopps.android.utils.Alert;
import com.geopps.android.utils.Constants;
import com.geopps.android.utils.GCalendar;
import com.geopps.android.utils.Tools;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Asynchronously load the events in a given calendar.
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
class AsyncUpdateLocalEventsInGoogleCalendar extends CalendarAsyncTask {
  
    AsyncUpdateLocalEventsInGoogleCalendar(CalendarSampleActivity calendarSample) {
	super(calendarSample);
    }

    @Override
    protected void doInBackground() throws IOException {
	
	// Get all the calendars and get the ID of the Geopps calendar
	CalendarList feed = client.calendarList().list().setFields(CalendarInfo.FEED_FIELDS).execute();
	model.reset(feed.getItems());
	
	CalendarInfo[] sortedArray = model.toSortedArray();

	String geoppsCalendarId = null;

	for (int i = 0; i < sortedArray.length; i++) {
	    if (sortedArray[i].summary.toLowerCase().equals(Constants.GOOGLE_CALENDAR_NAME_LOWERCASE)) {
		geoppsCalendarId = sortedArray[i].id;
	    }
	}
	
	// If (geoppsCalendarId == null) create the Calendar
	if(geoppsCalendarId==null){
	    
	}
	
	// Check all active alerts in the GCalendar table
	// When the user creates the alarm OR active it creates a new GCalendar entry
	// and sync it with the remote Google Calendar. Still, it can be possible that
	// Internet is not available at that moment. For that reason here we check if in the
	// GCalendar table the GCalendarID value is null. In that case in will try to upload 
	// the event to Google Calendar
	
	AlertsDataSource datasource = new AlertsDataSource(Tools.getContext());
	GCalendarDataSource gcalendardatasource = new GCalendarDataSource(Tools.getContext());
	
	gcalendardatasource.open();
	
	List<GCalendar> activeGCAlerts =  gcalendardatasource.getAllActiveGCalendar();
	
	for (GCalendar gc : activeGCAlerts){
	   
	    if(gc.getGCalerndarId().isEmpty()){
		
		//Create a Gcalendar Event and Add new entry
		//Calendar createdCalendar = null;
		
		try {
		    
		    datasource.open();
		    Alert a = datasource.getAlertById(gc.getLocalAlertId());
		    		    
		    Log.v("track","----2-----");
		    
		    Event newEvent = new Event();
		    
		    newEvent.setSummary(a.getComment());
		    newEvent.setLocation(a.getPlace().getName());

		    Date startDate = a.getStartDate().getTime();
		    Date endDate = a.getEndDate().getTime();
		    
		    DateTime start = new DateTime(startDate, TimeZone.getTimeZone(a.getTimezone()));
		    newEvent.setStart(new EventDateTime().setDateTime(start));
		    DateTime end = new DateTime(endDate, TimeZone.getTimeZone(a.getTimezone()));
		    newEvent.setEnd(new EventDateTime().setDateTime(end));

		    Log.v("track","----4-----" + startDate);

		    // Create the event on the Google Calendar
		    Event createdEvent =  client.events().insert(geoppsCalendarId, newEvent).execute();

		    // Update the GCalendar entry
		    gcalendardatasource.updateGCalendarId(createdEvent.getId(), gc.getId());
		    
		    Log.v("track","final tracker");

		    datasource.close();
		    
		} catch (Exception e){
		    Log.e("AsyncLoadEvents","Exception: " + e.toString());
		}
		
	    }
	}

	gcalendardatasource.close();
	
	// 1. Get remote events, check if all the id's are in the GCalendar table if not create a new Alert. 
	// 2. Check if the Place used is already in the database
	// 3. Notify the user that everything was ok OR place doesn't exist add or edit
	Events ee = client.events().list(geoppsCalendarId).execute();

	List<Event> lee = ee.getItems();

	for (int i = 0; i < lee.size(); i++) {
	   

	}
    }

    static void run(CalendarSampleActivity calendarSample) {
	new AsyncUpdateLocalEventsInGoogleCalendar(calendarSample).execute();
    }
}
