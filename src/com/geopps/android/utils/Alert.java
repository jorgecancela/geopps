package com.geopps.android.utils;

import java.util.Calendar;
import java.util.List;

import android.util.Log;

import com.geopps.android.db.AlertsDataSource;
import com.geopps.android.db.GCalendarDataSource;
import com.geopps.android.db.PlacesDataSource;

/**
 * This class implements a Alert object
 *
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class Alert {
    private long id;
    private String comment, flag_active, ringtone;
    private Calendar startDate, endDate;
    private String timezone;
    private int type;
    private int place_id;

    public long getId() {
	return this.id;
    }

    public String getComment() {
	if (this.comment == null) {
	    return "";
	} else {
	    return this.comment;
	}
    }

    public String getTimezone() {
	if (this.timezone == null) {
	    return "";
	} else {
	    return this.timezone;
	}
    }

    public String getRingtone() {
	if (this.ringtone == null) {
	    return "";
	} else {
	    return this.ringtone;
	}
    }

    public Calendar getStartDate() {
	return this.startDate;
    }

    public Calendar getEndDate() {
	return this.endDate;
    }

    public String getStartDateString() {
	return Tools.CalendarToStringToDb(this.startDate);
    }

    public String getStartEndString() {
	return Tools.CalendarToStringToDb(this.endDate);
    }


    public Place getPlace() {
	PlacesDataSource database = new PlacesDataSource(Tools.getContext());
	database.open();

	Place myPlace = database.getPlaceById(this.place_id);
	database.close();
	return myPlace;
    }

    public String getFlagActive() {
	return this.flag_active;
    }

    public Boolean getFlagActiveBoolean() {	
	try {	
	    
	    if(this.flag_active.equals(Constants.BOOLEAN_TRUE)) {
		return true;
	    } else {
		return false;
	    }
	    
	} catch(Exception e) {
	    return false;
	}
    }

    public int getType() {
	return this.type;
    }

    public void alert(long id, String comment, String ringtone, Calendar startDate, Calendar endDate, String timezone,
	    String flag_active, int type, int place_id) {

	this.id = id;
	this.startDate = startDate;
	this.endDate = endDate;
	this.timezone = timezone;
	this.comment = comment;
	this.ringtone = ringtone;
	this.type = type;
	this.place_id = place_id;

    }

    public void setId(long id) {
	this.id = id;
    }

    public void setType(int type) {
	this.type = type;
    }

    public void setPID(int pid) {
	this.place_id = pid;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public void setTimezone(String timezone) {
	this.timezone = timezone;
    }

    public void setRingtone(String ringtone) {
	this.ringtone = ringtone;
    }

    public void setFlagActive(String flagActive) {
	this.flag_active = flagActive;
    }

    public void setStartDate(String date) {
	this.startDate = Tools.StringToCalendarFromDb(date);  
    }

    public void setEndDate(String date) {
	this.endDate = Tools.StringToCalendarFromDb(date);  
    }

    public void setAlarmActive(Boolean setActive) {
	AlertsDataSource datasource = new AlertsDataSource(Tools.getContext());
	datasource.open();

	GCalendarDataSource gcDatasource = new GCalendarDataSource(Tools.getContext());
	gcDatasource.open();

	if (setActive){

	    Tools.activePendingAlerts();
	    datasource.setActive(this.id);
	    gcDatasource.setActive(this.id);
	    
	}else{

	    datasource.setActiveFalse(this.id); 
	    gcDatasource.setActiveFalse(this.id);
	    
	    // Poner en la tabla u NULL

	}

	datasource.close();	
	gcDatasource.close();
    }

    public void createRelatedTables(){

	try{
	    // GCalendarDataSource stores:
	    // [datetime, alert_id, google_calendar_remote_id, active_flag]
	    // then tries to create a remote Google Calendar Event, if that is possible add the remote ID
	    GCalendarDataSource gcDatasource = new GCalendarDataSource(Tools.getContext());

	    gcDatasource.open();
	    gcDatasource.createGCalendar(Tools.CalendarToStringToDb(Calendar.getInstance()), this.id, "", Constants.BOOLEAN_TRUE);

	    Log.v("Alert", "Esta es la tabla GCalendar ACTIVAS: [local_alert_id, remote_id, flag_active]");

	    List<GCalendar> gclist = gcDatasource.getAllActiveGCalendar();
	    for (int i=0;i<gclist.size();i++){
		Log.v("Alert", gclist.get(i).getLocalAlertId() + ", "+ gclist.get(i).getGCalerndarId() +", "+gclist.get(i).getFlagActive());
	    }

	    Log.v("Alert", "Esta es la tabla GCalendar NO ACTIVAS: [local_alert_id, remote_id, flag_active]");

	    gclist = gcDatasource.getAllNonActiveGCalendar();
	    for (int i=0;i<gclist.size();i++){
		Log.v("Alert", gclist.get(i).getLocalAlertId() + ", "+ gclist.get(i).getGCalerndarId() +", "+gclist.get(i).getFlagActive());
	    }
	    gcDatasource.close();

	}catch(Exception e){
	    Log.e("Alert", "Exception creating the GCalendar row: " + e.toString());
	}
    }

    public String toStringComment() {
	return this.comment;
    }

    public String toStringStartDate() {
	return Tools.CalendarDateToStringToShow(this.startDate);
    }

    public String toStringEndDate() {
	return Tools.CalendarDateToStringToShow(this.endDate);
    }

    public String toStringStartTime() {
	return Tools.CalendarTimeToStringToShow(this.startDate);
    }

    public String toStringEndTime() {
	return Tools.CalendarTimeToStringToShow(this.endDate);
    }

    public String toStringCommentLimited(int limited) {

	if (this.getComment().length() > limited) {

	    return this.comment.substring(1, limited) + "...";

	} else {

	    return this.comment;
	}
    }
}
