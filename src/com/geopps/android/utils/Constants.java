package com.geopps.android.utils;

/**
 * This class contains all the constants used within the the application  
 * except Constants values related to the Interoperability package
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class Constants {
    //SQLite3 does not support NaN as valid value
    public static final Double DOUBLE_NAN_VALUE_TO_STORE = Double.valueOf(9999999); 

    public static final String DATE_FORMAT_TO_STORE = "yyyy-MM-dd";
    public static final String TIME_FORMAT_TO_STORE = "HH:mm";
    public static final String DATE_FORMAT_TO_SHOW = "dd/MM/yyyy";
    public static final String DATE_AND_TIME_FORMAT_TO_STORE =  "yyyy-MM-dd'T'HH:mm";
    
    public static final String DATE_FORMAT_TO_INTEROPERABILITY = "MM/dd/yyyy";
    
    public static final String DOUBLE_FORMAT_TO_PRINT = "%.2f";
    
    public static final String LANGUAGE_ES = "es";
    public static final String LANGUAGE_EN = "en";
    public static final String LANGUAGE_FR = "fr";
    public static final String LANGUAGE_DEFAULT = "en";
       
    public static final String BOOLEAN_TRUE = "true";
    public static final String BOOLEAN_FALSE = "false";
    
    public static final String PLACE_ID_PARCEL_STRING = "placeID";
    public static final String ALERT_ID_PARCEL_STRING = "alertID";
    
    public static final int TEXT_LIMITED_20_CHARS = 20;
    public static final int TEXT_LIMITED_40_CHARS = 40;
    public static final int TEXT_LIMITED_50_CHARS = 50;
    
    // meters
    public static final int SMALL_RADIUS = 200;
    public static final int MEDIUM_RADIUS = 1000;
    public static final int LARGE_RADIUS = 2500;
    public static final int X_LARGE_RADIUS = 7000;
    
    public static final int DELAY_4_MINUTES = 240000;
    public static final int DELAY_2_MINUTES = 120000;
    public static final int DELAY_1_MINUTE = 60000;
    public static final int DELAY_45_SECONDS = 45000;
    public static final int DELAY_30_SECONDS = 30000;
    public static final int DELAY_20_SECONDS = 20000;
    public static final int DELAY_10_SECONDS = 10000;
    public static final int DELAY_1_SECOND = 1000;
    
    // Gps listener 
    public static final int MINTIME_30_SECONDS = 30000;
    public static final int MINDIST_5_METERS = 5;

    public static final int SMALL_ZOOM = 15;
    public static final int MEDIUM_ZOOM = 13;
    public static final int LARGE_ZOOM = 12;
    public static final int X_LARGE_ZOOM = 10;

    public static final int INITIAL_ZOOM = 4;
    
    public static final int ALERT_TYPE_ALARM = 1;
    public static final int ALERT_TYPE_NOTIFICATION = 2;
     
    public static final Double MARGIN_FACTOR_ALERT_SHOW_MAP = 3.5;
    
    public static final String MAP_TYPE_ID_PARCEL_STRING = "mapType";
    public static final int SHOW_ACTIVE_ALERTS = 1;
    public static final int SHOW_ALL_ALERTS = 2;
    public static final int SHOW_RAISED_ALERTS = 3;
    
    public static final String GOOGLE_CALENDAR_NAME_LOWERCASE = "geopps";
    public static final int MAX_GOOGLE_CALENDAR_YEAR = 2050;
    
    public static final int MAX_CALENDAR_YEAR = 2050;
    public static final int MAX_CALENDAR_MONTH = 12;
    public static final int MAX_CALENDAR_DAY = 31;
    
    public static final Boolean GCALENDAR_SYNC_FLAG = false;
}
