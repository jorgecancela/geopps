package com.geopps.android.utils;

import java.util.Calendar;

/**
 * This class implements a GCalendar object
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class GCalendar {
    private long id;
    private long localAlertId;
    private String gCalendarId, flagActive;
    private Calendar date; 

    public long getId() {
	return this.id;
    }
	
    public Long getLocalAlertId() {
	return this.localAlertId;
    }

    public String getGCalerndarId() {
	if (this.gCalendarId == null) {
	    return "";
	} else {
	    return this.gCalendarId;
	}
    }

    public Calendar getDate() {
	return this.date;
    }

    public String getDateString() {
	return Tools.CalendarToStringToDb(this.date);
    }


    public String getFlagActive() {
	return this.flagActive;
    }

    public Boolean getFlagActiveBoolean() {	
	try {	
	    if(this.flagActive.equals(Constants.BOOLEAN_TRUE)) {
		return true;
	    } else {
		return false;
	    }
	} catch(Exception e) {
	    return false;
	}
    }

    public void gCalendar(long id, Calendar date, long localAlertId, String gCalendarId, String flagActive) {

	this.id = id;
	this.date = date;
	this.localAlertId = localAlertId;
	this.gCalendarId = gCalendarId;
	this.flagActive = flagActive;

    }


    public void setId(long id) {
	this.id = id;
    }

    public void setLocalAlertId(long localAlertId) {
	this.localAlertId = localAlertId;
    }

    public void setGCalendarId(String gCalendarId) {
	this.gCalendarId = gCalendarId;
    }

    public void setFlagActive(String flagActive) {
	this.flagActive = flagActive;
    }

    public void setDate(String date) {
	this.date = Tools.StringToCalendarFromDb(date);  
    }

}
