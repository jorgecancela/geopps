package com.geopps.android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.geopps.android.AlertDialogActivity;

/**
 * This class is a BroadcastReceiver. It is listening for a signal
 * to raise an alert
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class MyAlarmReceiver extends BroadcastReceiver { 
    private String LOGGER_TAG = "MyAlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

	try{

	    Intent iAlarm = new Intent(context, AlertDialogActivity.class ); 
	    iAlarm.addFlags(Intent.FLAG_FROM_BACKGROUND); 
	    iAlarm.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);   // Added 
	    iAlarm.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
	    context.startActivity(iAlarm); 

	}catch(Exception e){
	    Log.e(LOGGER_TAG, "Exception: " + e.toString());
	}
    }
}