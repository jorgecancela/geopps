package com.geopps.android.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.osmdroid.util.GeoPoint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import com.geopps.android.db.AlertsDataSource;

/**
 *  This thread is the core of the geopps app. It starts a background thread which updates the user location
 *  every Tools.timerUpdateMilliseconds time. 
 *  
 *  Every time the thread is waken up:
 *  
 *  - it calculates the distance to each alert 
 *  - it updates the Tools.timerUpdateMilliseconds of the timer according of: user velocity and distance to the closest alert
 *  - it rises all the alarms
 *  
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class PeriodicTimerService extends Service {
    private Handler mHandler = new Handler();
    private LocationManager lm;
    private boolean gps_enabled=false;
    private boolean network_enabled=false;
    private List<Alert> activeAlerts = null;
    private String LOGGER_TAG = "PeriodicTimerService";
    private Boolean flagFirstTime = true;
    private Boolean flagFirstLecture = true;
    
    private Runnable periodicTask = new Runnable() {
	public void run() {	    

	    // periodicTask updates the Location value. Checks if any target has been reached
	    // trigger the associated activities and updates Tools.timerUpdateMilliseconds
	    // according to the user position

	    if(Tools.moreAlertsPending||flagFirstTime){

		try{

		    periodicTask(); 

		}catch(Exception e){
		    Log.e(LOGGER_TAG, "Exception onPeriodictask: " + e.toString());	
		}

		Log.v(LOGGER_TAG, "Ahora entra  " + Tools.timerUpdateMilliseconds);

		mHandler.postDelayed(periodicTask, 45000);

	    }else{

		try{
		    
		    lm.removeUpdates(locationListenerNetwork);

		}catch(Exception e){
		    Log.e(LOGGER_TAG, "Exception: " + e.toString());	
		}
		
		try{

		    lm.removeUpdates(locationListenerGps);

		}catch(Exception e){
		    Log.e(LOGGER_TAG, "Exception: " + e.toString());	
		}

		//Log.v(LOGGER_TAG, "Ahora empieza el delay de 2 minutos");
		//mHandler.postDelayed(periodicTask, Constants.DELAY_2_MINUTES);
		stopService();
	    }
	}
    };

    @Override
    public void onCreate() {
	// onCreate updates the user position value and starts the periodic task
	Tools.setContext(this);
	updateLocationServices(Tools.getContext());
	mHandler.postDelayed(periodicTask, Constants.DELAY_1_SECOND);
    }

    @Override
    public void onDestroy() {

	try{

	    mHandler.removeCallbacks(periodicTask);

	}catch(Exception e){
	    Log.e(LOGGER_TAG, "Exception: " + e.toString());	
	}

	super.onDestroy();
    }

    private void stopService(){
	this.stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
	// TODO Auto-generated method stub
	return null;
    }

    /**
     *  periodicTask is called in every cycle of the PeriodicTimerService:
     *  - update the Location value.
     *  - open database and get all the active alerts
     *  - trigger the associated activities: syncGCalendar  
     *  - update Tools.timerUpdateMilliseconds according to the user position
     */
    public void periodicTask() {

	// updates the location services and evaluates which is the best Location value
	activeAlerts = updateActiveAlerts();

	if (flagFirstTime||(activeAlerts.size() > 0)){

	    updateLocationServices(Tools.getContext());

	    if (Tools.lastLocation != null) {

		flagFirstTime = false;

		Location newLocation = checkForNewLocation();

		// if the value is not null handle the new Location value
		if(newLocation != null){
		    handleUpdatedLocation(newLocation);
		}

		// If sync with Google Calendar is active 
		// Then check if all the entries in the local database have a remote Google Calendar ID 
		// It could be that during the upload process the internet connection was not available
		Tools.updateFlags();

		if(Tools.syncGCalendar){

		}
	    }

	}else{

	    Tools.moreAlertsPending = (activeAlerts.size() > 0);

	}
    }

    /**
     * Called when a Location different from null is detected
     * - Check if the user is within any alert
     * - Raise alerts (if needed)
     * - Update the Tools.timerUpdateMilliseconds value according to the user location
     * 
     * @param location
     */
    private void handleUpdatedLocation(Location location){

	raiseAlarms(checkAlerts(activeAlerts));

	// Calculate the distance to each of the alerts taking into
	// account the GPS precision

	// Update next timer
	//
	// if distance == 0m 30seconds
	// if 0 < distance < 500 30-45 seconds
	// if 500 < distance < 1000 45-60 seconds
	//
	// tracking est�ndar t = ((10/500)*d)*1000 + 30000
	// tracking r�pido t = ((1/500)*d)*1000 + 30000

	// Raise alarms if needed
	// if(NO ALERTS ACTIVE) timer = 60min

	if (Tools.lastLocation == null){

	    Tools.timerUpdateMilliseconds = Constants.DELAY_10_SECONDS;

	}else{

	    Tools.timerUpdateMilliseconds = (int) ((10/500)*Tools.closestDistance)*1000 + Constants.DELAY_1_MINUTE;

	    if (Tools.timerUpdateMilliseconds > Constants.DELAY_2_MINUTES){
		Tools.timerUpdateMilliseconds = Constants.DELAY_2_MINUTES;
	    }

	}
    }

    private List<Alert> updateActiveAlerts(){
	List<Alert> localActiveAlerts = null;

	// Check all the active alerts
	AlertsDataSource datasource = new AlertsDataSource(Tools.getContext());

	datasource.open();

	try {

	    localActiveAlerts = datasource.getAllActiveAlerts();
	    datasource.close();

	    // Check time
	    for (int i= 0; i < localActiveAlerts.size(); i++){

		Alert a = localActiveAlerts.get(i);

		if(!Tools.todayIsWithinTheseDates(a.getStartDate(), a.getEndDate())){
		    localActiveAlerts.remove(i);
		}	

	    }

	} catch (Exception e) {

	    Tools.timerUpdateMilliseconds = Constants.DELAY_30_SECONDS;
	    datasource.close();

	}

	return localActiveAlerts;
    }

    private ArrayList<Integer> checkAlerts(List<Alert> currentActiveAlerts) {

	ArrayList<Float> distances = new ArrayList<Float>();
	ArrayList<Integer> alertsInRange = new ArrayList<Integer>();
	Float minValue = Float.MAX_VALUE;

	//
	// Closest distance
	//
	for (int i = 0; i < currentActiveAlerts.size(); i++) {
	    Alert a = currentActiveAlerts.get(i);

	    if (Tools.lastLocation != null) {

		Place p = a.getPlace();
		GeoPoint gp = new GeoPoint(p.getLat() / 1E6, p.getLon() / 1E6);

		distances.add(Tools.getDistanceBetweenGeoPoints(Tools.lastGeoPoint, gp) - p.getRadius());

		if (distances.get(i) <= 0) {

		    alertsInRange.add((int) a.getId());    

		}

		if(distances.get(i) < minValue){

		    minValue = distances.get(i);

		}
	    }
	}

	//
	// Closest time 
	// si todas tienen un tiempo. poner el timer para ese momento
	// comprobar que no se supera el limite de tiempo Interger.MAX_VALUE
	// comprobar que se incluyen 
	//

	// Update closest distance
	Tools.closestDistance = minValue;

	return alertsInRange;
    }

    private void raiseAlarms(ArrayList<Integer> alertsInRange) {

	if (alertsInRange.size() > 0) {
	    Tools.alertsInRange = alertsInRange;
	    showMessage();
	}
    }

    private void showMessage() {
	try {

	    Tools.getContext();
	    AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
	    Intent intent = new Intent(this.getApplicationContext(), MyAlarmReceiver.class);
	    PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 0, intent, 0);

	    Calendar time = Calendar.getInstance();
	    time.setTimeInMillis(System.currentTimeMillis());
	    time.add(Calendar.SECOND, 0);
	    alarmMgr.set(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), pendingIntent);

	} catch (Exception e) {

	    Log.e(LOGGER_TAG, "Exception: " + e.toString());

	}
    }

    private LocationListener locationListenerGps = new LocationListener() {
	public void onLocationChanged(Location location) {

	    Tools.lastGeoPoint = new GeoPoint(location);
	    Tools.lastLocation = location;

	    lm.removeUpdates(this);
	    lm.removeUpdates(locationListenerNetwork);

	}

	public void onProviderDisabled(String provider) {}
	public void onProviderEnabled(String provider) {}
	public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    private LocationListener locationListenerNetwork = new LocationListener() {
	public void onLocationChanged(Location location) {

	    Tools.lastGeoPoint = new GeoPoint(location);
	    Tools.lastLocation = location;

	    lm.removeUpdates(this);
	    lm.removeUpdates(locationListenerNetwork);

	}

	public void onProviderDisabled(String provider) {}
	public void onProviderEnabled(String provider) {}
	public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    /**
     * It activates the location providers and request for an location update
     */
    private boolean updateLocationServices(Context context)
    {
	try{

	    if(lm==null)
		lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

	    try{
		gps_enabled=lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
	    }catch(Exception ex){
		Log.e(LOGGER_TAG, "Exception enablig GPS_PROVIDER");
	    }

	    try{
		network_enabled=lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	    }catch(Exception ex){
		Log.e(LOGGER_TAG, "Exception enablig NETWORK_PROVIDER");
	    }

	    //don't start listeners if no provider is enabled
	    if(!gps_enabled && !network_enabled)
		return false;

	    if(gps_enabled)
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);

	    if(network_enabled)
		lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);

	}catch(Exception e){
	    Log.e(LOGGER_TAG,"Error: " + e.toString());
	}

	return true;
    }

    /**
     * It evaluates the all the locations values from the different providers
     * and returns the most recent location
     */
    private Location checkForNewLocation() {
	//if(Tools.lastLocation != null){
	//lm.removeUpdates(locationListenerGps);
	//lm.removeUpdates(locationListenerNetwork);
	//}
	
	Location net_loc=null, gps_loc=null;
	
	// To speed up the first case
	if (flagFirstLecture){
	    flagFirstLecture = false;
	    
	    net_loc=lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	    
	    if (net_loc != null){
		return net_loc;
	    }
	}

	if(gps_enabled){
	    gps_loc=lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	}

	if(network_enabled){
	    net_loc=lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	}

	//if both values are available return the most recent one
	if(gps_loc!=null && net_loc!=null){
	    if((gps_loc.getTime()>net_loc.getTime())&&(gps_loc.getAccuracy() < 2000.00)){
		return gps_loc;
	    }else{
		return net_loc;
	    }
	}

	if((gps_loc!=null)&&(gps_loc.getAccuracy() < 2000.00)){	   
	    return gps_loc;
	}

	if(net_loc!=null){	    
	    return net_loc;
	}

	return Tools.lastLocation;
	//return null;
    }

}
