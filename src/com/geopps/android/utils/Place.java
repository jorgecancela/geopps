package com.geopps.android.utils;

/**
 * This class implements a Place object
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class Place {
    private long id;
    private String name, address;
    private long lon, lat, radius;

    public long getId() {
	return this.id;
    }
    
    public String getName() {
	return this.name;
    }

    public String getAddress() {	
	return this.address;
    }

    public long getLat() {
	return this.lat;
    }

    public long getLon() {
	return this.lon;
    }
    
    public long getRadius() {
	return this.radius;
    }

    public void patient(long id, String name, String address,
	    long lat, long lon, long radius) {

	this.id = id;
	this.name = name;
	this.address = address;
	this.lat = lat;
	this.lon = lon;
	this.radius = radius;
    }

    public void setId(long id) {
	this.id = id;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setAddress(String address) {
	this.address = address;
    }

    public void setLon(long lon) {
	this.lon = lon;
    }
    
    public void setLat(long lat) {
	this.lat = lat;
    }
    
    public void setRadius(long radius) {
	this.radius = radius;
    }
    
    @Override
    public String toString() {
	return name;
    }
}
