package com.geopps.android.utils;

import java.util.List;

import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class RealTimeUserTrack extends AsyncTask<Void, String, Void> {
    private boolean running = true;
    private SelectAreaItemizedOverlay userOverlay = null;
    private MapView mapView = null;
    private List<GeoPoint> alertsToShow = null;
    private Boolean averageCenter = true;
    private Boolean firstTime = true;
    private String LOGGER_TAG = "RealTimeUserTrack";

    @Override
    protected Void doInBackground(Void... params) {
	while( running ) {
	    //fetch data from server;
	    try {
		publishProgress();
		Thread.sleep(10000);

	    } catch (Exception e) {
		Log.e(LOGGER_TAG, "Exception: " +  e.toString());
	    }
	}

	return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
	if(userOverlay == null){
		Log.v("IN", "NULL");   
	}
	Log.v("IN", "NO NULL"); 
	Log.v("IN", "Listado de alerts in range: " + userOverlay.toString());
	Log.v("IN", "Listado de alerts in range: " + mapView.toString());
	Log.v("IN", "Listado de alerts in range: " + alertsToShow.size());
	Log.v("IN", "Listado de alerts in range: " + firstTime);

	BoundingBoxE6 bbE = Tools.updateMapView(userOverlay, mapView, alertsToShow, averageCenter, firstTime);	
	firstTime = false;

	Log.v("IN", "No llega a salir. bbE " + bbE.toString());
	if (bbE != null){
	    mapView.getController().setCenter(bbE.getCenter());
	    mapView.zoomToBoundingBox(bbE);
	    mapView.invalidate();
	}

	if( ! running ) {
	    return; 
	}
    }

    public void stop() {
	running = false;
    }

    public void setupTracker(SelectAreaItemizedOverlay userOverlay, MapView mapView, List<GeoPoint> activeAlerts, Boolean averageCenter, Boolean firstTime){
	this.userOverlay = userOverlay;
	this.mapView = mapView;
	this.alertsToShow = activeAlerts;
	this.averageCenter = averageCenter;
	this.firstTime = firstTime;
    }
}