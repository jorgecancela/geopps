package com.geopps.android.utils;

import java.util.ArrayList;

import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IMapView;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.MapView.Projection;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.OverlayItem.HotspotPlace;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;

import com.geopps.android.PlacesActivityAdd;

/**
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class SelectAreaItemizedOverlay extends ItemizedOverlay<OverlayItem> {
    private ArrayList<OverlayItem> overlayItemList = new ArrayList<OverlayItem>();
    private Boolean mapIsTouchable = true;
    private GeoPoint point;
    private Paint paint1, paint2;
    private float radius; // in meters
    private Boolean bottomCenter = false;

    public SelectAreaItemizedOverlay(Drawable pDefaultMarker, ResourceProxy pResourceProxy) {
	super(pDefaultMarker, pResourceProxy);
    }
       
    public void addItem(GeoPoint p, String title, String snippet) {
	OverlayItem newItem = new OverlayItem(title, snippet, p);
	
	if (bottomCenter){
	newItem.setMarkerHotspot(HotspotPlace.CENTER);
	}
	
	overlayItemList.add(newItem);
	populate();
    }

    public void clear() {
	overlayItemList.clear();
    }
    
    public void setBottomCenter(Boolean flag) {
	bottomCenter = flag;
    }

    public void setArea(GeoPoint point, float radius) {
	this.point = point;

	paint1 = new Paint();
	paint1.setARGB(200, 5, 125, 159);
	paint1.setStrokeWidth(2);
	paint1.setStrokeCap(Paint.Cap.ROUND);
	paint1.setAntiAlias(true);
	paint1.setDither(false);
	paint1.setStyle(Paint.Style.STROKE);

	paint2 = new Paint();
	paint2.setARGB(60, 5, 125, 159);

	this.radius = radius;
    }
    
    @Override 
    public boolean onLongPress(MotionEvent event, MapView mapView) { 
	// final int action = event.getAction();
	boolean result = false;

	if (mapIsTouchable){

	    final int x = (int) event.getX();
	    final int y = (int) event.getY();
	    final Projection pj = mapView.getProjection();

	    overlayItemList.clear();

	    GeoPoint pt = (GeoPoint) pj.fromPixels(x, y);
	    
	    PlacesActivityAdd.currentPoint = pt;

	    mapView.getController().animateTo(pt);
	    overlayItemList.add(new OverlayItem("Title", "Snippet", pt));
	    populate();

	    setArea(pt, PlacesActivityAdd.currentRadius);
	}

	PlacesActivityAdd.changeSearchPanelToAddPanel();

	return result;
    }

    @Override
    protected OverlayItem createItem(int arg0) {
	// TODO Auto-generated method stub
	return overlayItemList.get(arg0);
    }

    @Override
    public int size() {
	return overlayItemList.size();
    }
    
    @Override
    public void draw(Canvas c, MapView osmv, boolean shadow) {
	super.draw(c, osmv, shadow);

	if (point != null) {
	    Point pt = osmv.getProjection().toPixels(point, null);
	    float projectedRadius = osmv.getProjection().metersToEquatorPixels(
		    radius);

	    c.drawCircle(pt.x, pt.y, projectedRadius, paint2);
	    c.drawCircle(pt.x, pt.y, projectedRadius, paint1);
	}
    }
    
    
    public void setTouchable(Boolean isTouchable) {
	 this.mapIsTouchable = isTouchable;
    }
    
    public Boolean getTouchable() {
	 return this.mapIsTouchable;
   }

    @Override
    public boolean onSnapToItem(int arg0, int arg1, Point arg2, IMapView arg3) {
	// TODO Auto-generated method stub
	return false;
    }
}