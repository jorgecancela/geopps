package com.geopps.android.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This class implements a Alert object
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class Settings {
    private long id;
    private String residence, email, username, password, language, twitter,
	    facebook, foursquare, pinterest, gcalendar, gcalendarId;
    private Date date;

    public long getId() {
	return this.id;
    }

    public String getResidence() {
	if (this.residence == null) {
	    return "";
	} else {
	    return this.residence;
	}
    }

    public String getEmail() {
	if (this.email == null) {
	    return "";
	} else {
	    return this.email;
	}
    }

    public String getUsername() {
	if (this.username == null) {
	    return "";
	} else {
	    return this.username;
	}
    }

    public String getPassword() {
	if (this.password == null) {
	    return "";
	} else {
	    return this.password;
	}
    }

    public String getLanguage() {
	if (this.language == null) {
	    return "";
	} else {
	    return this.language;
	}
    }

    public String getTwitter() {
	if (this.twitter == null) {
	    return "";
	} else {
	    return this.twitter;
	}
    }

    public String getFacebook() {
	if (this.facebook == null) {
	    return "";
	} else {
	    return this.facebook;
	}
    }

    public String getFoursquare() {
	if (this.foursquare == null) {
	    return "";
	} else {
	    return this.foursquare;
	}
    }

    public String getPinterest() {
	if (this.pinterest == null) {
	    return "";
	} else {
	    return this.pinterest;
	}
    }
    
    public String getGCalendar() {
	return this.gcalendar;
    }
    
    public String getGCalendarId() {
	return this.gcalendarId;
    }

    public Boolean getGCalendarBoolean() {	
	try {	
	    
	    return this.gcalendar.equals(Constants.BOOLEAN_TRUE);
		    
	} catch(Exception e) {
	    return false;
	}
    }

    public String getDateStored() {		
	SimpleDateFormat textFormat = new SimpleDateFormat(Constants.DATE_FORMAT_TO_STORE);

	return textFormat.format(this.date);
    }

    public void settings(long id, String residence, String email,
	    String username, String password, String language, String twitter,
	    String facebook, String foursquare, String pinterest, String gcalendar, String gcalendarId ,Date date) {

	this.id = id;
	this.residence = residence;
	this.email = email;
	this.username = username;
	this.password = password;
	this.language = language;
	this.twitter = twitter;
	this.facebook = facebook;
	this.foursquare = foursquare;
	this.pinterest = pinterest;
	this.gcalendar = gcalendar;
	this.gcalendarId = gcalendarId;
	this.date = date;
    }

    public void setId(long id) {
	this.id = id;
    }

    public void setResidence(String residence) {
	this.residence = residence;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public void setLanguage(String language) {
	this.language = language;
    }

    public void setTwitter(String twitter) {
	this.twitter = twitter;
    }

    public void setFacebook(String facebook) {
	this.facebook = facebook;
    }

    public void setFoursquare(String foursquare) {
	this.foursquare = foursquare;
    }

    public void setPinterest(String pinterest) {
	this.pinterest = pinterest;
    }

    public void setGCalendar(String gcalendar) {
	this.gcalendar = gcalendar;
    }

    public void setGCalendarId(String gcalendarId) {
	this.gcalendarId = gcalendarId;
    }
    
    public void setDate(String date) {
	SimpleDateFormat textFormat = new SimpleDateFormat(
		Constants.DATE_FORMAT_TO_STORE);

	try {
	    this.date = textFormat.parse(date);
	} catch (Exception ex) {
	    this.date = Calendar.getInstance().getTime();
	}
	
    }

    @Override
    public String toString() {
	return date.toString();
    }
}
