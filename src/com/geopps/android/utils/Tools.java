package com.geopps.android.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import com.geopps.android.R;
import com.geopps.android.db.SettingsDataSource;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * This class is a set of common tools for the whole project
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class Tools {
    private static Context context;
    public static GeoPoint lastGeoPoint = null;
    public static Location lastLocation = null;

    public static boolean moreAlertsPending = true;
    public static boolean syncGCalendar = false;

    public static int timerUpdateMilliseconds = 0;
    public static Boolean firstTime = true;
    public static PeriodicTimerService periodicTimerService;

    public static ArrayList<Integer> alertsInRange = null;
    public static ArrayList<Alert> alertsRaised = null;
    public static Float closestDistance = Float.valueOf(0);

    public static MediaPlayer mMediaPlayer;

    private static String LOGGER_TAG = "Tools";

    public static void init() {
	Log.v(LOGGER_TAG, "init................");
	try { 

	    if (firstTime) {
		    firstTime = false;
		    context.startService(new Intent(context, PeriodicTimerService.class));
		}

	} catch (Exception e) {
	    Log.e(LOGGER_TAG, "Error on init" + e.toString());
	}
    }

    public static Calendar StringToCalendarFromDb(String myDate) {

	SimpleDateFormat textFormat = new SimpleDateFormat(Constants.DATE_AND_TIME_FORMAT_TO_STORE);
	Date partial = new Date();
	Calendar returnCalendar = new GregorianCalendar();

	try {
	    partial = textFormat.parse(myDate);
	    returnCalendar.setTime(partial);
	} catch (Exception ex) {
	    return null;
	}

	return returnCalendar;
    }

    public static String CalendarToStringToDb(Calendar myDate) {

	SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_AND_TIME_FORMAT_TO_STORE);
	String sDate = formatter.format(myDate.getTime());

	return sDate;
    }

    public static String CalendarInstanceToStringToDb() {

	SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_AND_TIME_FORMAT_TO_STORE);
	String sDate = formatter.format(Calendar.getInstance().getTime());

	return sDate;
    }

    public static String CalendarDateToStringToShow(Calendar myDate) {

	SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT_TO_SHOW);
	String sDate = formatter.format(myDate.getTime());

	return sDate;
    }

    public static String CalendarTimeToStringToShow(Calendar myDate) {

	SimpleDateFormat formatter = new SimpleDateFormat(Constants.TIME_FORMAT_TO_STORE);
	String sDate = formatter.format(myDate.getTime());

	return sDate;
    }

    public static Context getContext() {
	return context;
    }

    public static String getStringResource(int myString) {
	return context.getString(myString);
    }

    public static Calendar IntegersToCalendar(int year, int month, int day) {

	Calendar returnDate = new GregorianCalendar();
	returnDate.set(Calendar.YEAR, year);
	returnDate.set(Calendar.MONTH, month);
	returnDate.set(Calendar.DAY_OF_MONTH, day);

	return returnDate;
    }

    /** Check the availability of the Internet connection */
    public static boolean isOnline() {
	try {

	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();

	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
		return true;
	    }

	    return false;

	} catch (Exception e) {
	    return false;
	}
    }

    /**
     * Set a context in the class. Tools is not an Activity class, setting up a
     * context is extremely useful to the resources (language, strings...)
     */
    public static void setContext(Context mcontext) {
	if (context == null) {
	    context = mcontext;
	}
    }

    public static void setSettings(Context myContext) {
	SettingsDataSource  s_datasource = new SettingsDataSource(myContext); 
	Settings currentSet = new Settings();

	try { 

	    s_datasource.open();
	    currentSet = s_datasource.getLastSettings();

	} catch (Exception e) {
	    Log.e(LOGGER_TAG, "Error opening Settings datasource" + e.toString());
	}

	if (currentSet.getLanguage() == null) {
	    setLanguageApp("", myContext);
	} else {
	    setLanguageApp(currentSet.getLanguage().toString(), myContext);
	}	

	try {
	    s_datasource.close();
	} catch (Exception e) {
	    Log.e(LOGGER_TAG, "Error closing Settings datasource" + e.toString());
	}
    }

    public static void setLanguageApp(String lan, Context myContext) {
	Locale locale = new Locale(lan);
	// Locale.setDefault(locale);

	Configuration config = new Configuration();
	config.locale = locale;

	myContext.getResources().updateConfiguration(config, myContext.getResources().getDisplayMetrics());
    }

    /** Prints a text message in a Toast element **/
    public static void showToastMessageAndLog(Context context, String text, String logEntryName, String logEntryBody) {
	Toast.makeText(context, text, Toast.LENGTH_LONG).show();
	Log.v(LOGGER_TAG, "Error showing Toast message: " + logEntryName + " " + logEntryBody);
    }

    public static void showToastMessage(Context context, String text) {
	Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void showToastMessageMoreTime(Context context, String text) {
	Toast.makeText(context, text, 2 * Toast.LENGTH_LONG).show();
    }

    public static int calculateZoomLevel(long radius){
	if(radius < (Constants.X_LARGE_RADIUS/3)){
	    return Constants.SMALL_ZOOM;
	}

	if((radius < (2*Constants.X_LARGE_RADIUS/3))&&(radius > (Constants.X_LARGE_RADIUS/3))){
	    return Constants.MEDIUM_ZOOM;
	}

	if(radius > (2*Constants.X_LARGE_RADIUS/3)){
	    return Constants.LARGE_ZOOM;
	}

	return Constants.LARGE_ZOOM;
    }

    public static float getDistanceBetweenGeoPoints(GeoPoint p1, GeoPoint p2) {
	return 	p1.distanceTo(p2);
    }

    // TODO(jorge) externalize strings
    public static String getDistanceToPrint(GeoPoint p1, GeoPoint p2) {
	float d = getDistanceBetweenGeoPoints(p1, p2);
	String resultString = " ";

	if (d > 1000) {
	    return resultString + String.format(getStringResource(R.string.tools_distance_pre) + " " +  Constants.DOUBLE_FORMAT_TO_PRINT, (d / 1000)) + " " +  getStringResource(R.string.tools_km_away);
	} else {
	    return resultString + String.format(getStringResource(R.string.tools_distance_pre) + " " +  Constants.DOUBLE_FORMAT_TO_PRINT, d)  + " " +  getStringResource(R.string.tools_m_away);
	}
    }

    // TODO(jorge) externalize strings
    public static String getRadiousToPrint(long radious) {

	String resultString = " ";

	if (radious > 1000) {
	    return resultString + String.format(getStringResource(R.string.tools_size) + " " + Constants.DOUBLE_FORMAT_TO_PRINT, (Double.valueOf(radious)  / 1000)) + " " +  getStringResource(R.string.tools_distance_km);
	} else {
	    return resultString + String.format(getStringResource(R.string.tools_size) + " " + Constants.DOUBLE_FORMAT_TO_PRINT, Double.valueOf(radious) )  + " " +  getStringResource(R.string.tools_distance_m);
	}
    }

    public static Boolean geoPointsAreEqual(GeoPoint p1, GeoPoint p2){
	if ((p1 == null )||(p2 == null)){
	    return false;
	}else{
	    return ( (p1.getLatitudeE6() == p2.getLatitudeE6()) && (p1.getLongitudeE6() == p2.getLongitudeE6()) );
	}
    }

    public static BoundingBoxE6 calculateBoundingBoxE6(List<GeoPoint> myGeoList, Double marginFactor){
	// Set inverse max/min start values 
	int maxLat = (int) (-90 * 1E6);
	int maxLon = (int) (-180 * 1E6);
	int minLat = (int) (90 * 1E6);
	int minLon = (int) (180 * 1E6);

	// Find the max/min values
	for (GeoPoint gp : myGeoList) {
	    maxLat = Math.max(maxLat, gp.getLatitudeE6());
	    maxLon = Math.max(maxLon, gp.getLongitudeE6());
	    minLat = Math.min(minLat, gp.getLatitudeE6());
	    minLon = Math.min(minLon, gp.getLongitudeE6());
	}

	return new BoundingBoxE6(maxLat, maxLon, minLat, minLon);
    }

    public static GeoPoint setCenterGeoPoint(List<GeoPoint> myGeoList){
	// Set inverse max/min start values 
	int maxLat = (int) (-90 * 1E6);
	int maxLon = (int) (-180 * 1E6);
	int minLat = (int) (90 * 1E6);
	int minLon = (int) (180 * 1E6);

	// Find the max/min values
	for (GeoPoint gp : myGeoList) {
	    maxLat = Math.max(maxLat, gp.getLatitudeE6());
	    maxLon = Math.max(maxLon, gp.getLongitudeE6());
	    minLat = Math.min(minLat, gp.getLatitudeE6());
	    minLon = Math.min(minLon, gp.getLongitudeE6());
	}

	GeoPoint point1 = new GeoPoint(maxLat, maxLon);
	GeoPoint point2 = new GeoPoint(minLat, minLon);

	return GeoPoint.fromCenterBetween(point1, point2);
    }

    public static int setZoomLevel(List<GeoPoint> myGeoList){		
	int maxLat = (int) (-90 * 1E6);
	int maxLon = (int) (-180 * 1E6);
	int minLat = (int) (90 * 1E6);
	int minLon = (int) (180 * 1E6);

	// Find the max/min values
	for (GeoPoint gp : myGeoList) {
	    maxLat = Math.max(maxLat, gp.getLatitudeE6());
	    maxLon = Math.max(maxLon, gp.getLongitudeE6());
	    minLat = Math.min(minLat, gp.getLatitudeE6());
	    minLon = Math.min(minLon, gp.getLongitudeE6());
	}

	//int spanLat = (maxLat - minLat)/2;
	//int spanLon = (maxLon - minLon)/2;

	GeoPoint point1 = new GeoPoint(maxLat, maxLon);
	GeoPoint point2 = new GeoPoint(minLat, minLon);

	int maxSpan = point1.distanceTo(point2);

	int ONE_KM = 1000;

	if(maxSpan < ONE_KM){  return 16; }
	if(maxSpan < 5*ONE_KM){  return 15; }
	if(maxSpan < 10*ONE_KM){  return 14; }
	if(maxSpan < 20*ONE_KM){  return 13; }
	if(maxSpan < 40*ONE_KM){  return 12; }
	if(maxSpan < 80*ONE_KM){  return 11; }
	if(maxSpan < 120*ONE_KM){  return 10; }
	if(maxSpan < 240*ONE_KM){  return 9; }
	if(maxSpan < 480*ONE_KM){  return 8; }
	if(maxSpan < 960*ONE_KM){  return 7; }
	if(maxSpan < 1920*ONE_KM){  return 6; }
	if(maxSpan < 3840*ONE_KM){  return 5; }
	if(maxSpan < 7680*ONE_KM){  return 4; }
	if(maxSpan < 15360*ONE_KM){  return 3; }
	if(maxSpan < 30720*ONE_KM){  return 2; }
	if(maxSpan < 61440*ONE_KM){  return 1; }

	return 1;
    }

    public static void playSound(Context context, Uri alert) {

	mMediaPlayer = new MediaPlayer();
	try {

	    mMediaPlayer.setDataSource(context, alert);
	    final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

	    if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
		mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
		mMediaPlayer.prepare();
		mMediaPlayer.setLooping(true);
		mMediaPlayer.start();
	    }

	} catch (Exception e) {
	    Log.e(LOGGER_TAG, "Error playing sound: " + e.toString());
	}
    }

    public static BoundingBoxE6 updateMapView(SelectAreaItemizedOverlay userOverlay, MapView mapView, List<GeoPoint> activeAlerts, Boolean averageCenter, Boolean firstTime){

	ArrayList<GeoPoint> geoPointsToShow = new ArrayList<GeoPoint>();

	try {

	    geoPointsToShow = new ArrayList<GeoPoint>();

	    geoPointsToShow.clear();	    
	    geoPointsToShow.addAll(activeAlerts);

	    if(Tools.lastGeoPoint != null){

		geoPointsToShow.add(Tools.lastGeoPoint);

		userOverlay.clear();
		userOverlay.addItem(Tools.lastGeoPoint, "","");

	    }

	} catch (Exception e) {
	    Log.e(LOGGER_TAG,"Exception updateMapView e: "  + e.toString());
	    return null;
	}   

	return BoundingBoxE6.fromGeoPoints(geoPointsToShow);
    }

    public static void setTimeZoneSpinner(Context mContext, Spinner TZone){

	String[]TZ = TimeZone.getAvailableIDs();

	ArrayAdapter <CharSequence> adapter = new ArrayAdapter <CharSequence> (mContext, android.R.layout.simple_spinner_item, TZ);
	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

	TZone.setAdapter(adapter);

	for(int i = 0; i < TZ.length; i++) {

	    if(TZ[i].equals(TimeZone.getDefault().getID())) {
		TZone.setSelection(i);
	    }
	}
    }

    public static Boolean isToday(Calendar date){

	Calendar dateRef = Calendar.getInstance();

	if(date.get(Calendar.YEAR) == dateRef.get(Calendar.YEAR)&&
		date.get(Calendar.MONTH) == dateRef.get(Calendar.MONTH)&&
		date.get(Calendar.DAY_OF_MONTH) == dateRef.get(Calendar.DAY_OF_MONTH)){
	    return true;
	}

	return false;
    }

    public static Boolean todayIsWithinTheseDates(Calendar startDate, Calendar endDate){

	Calendar dateRef = Calendar.getInstance();

	return (dateRef.before(endDate)&&dateRef.after(startDate));
    }

    public static void updateFlags(){

	try{

	    SettingsDataSource datasource = new SettingsDataSource(Tools.getContext());

	    datasource.open();
	    Settings currentSettings = datasource.getLastSettings();
	    datasource.close();   

	    syncGCalendar = currentSettings.getGCalendarBoolean();

	}catch(Exception e){
	    Log.e(LOGGER_TAG,"Error at updateFlag: " + e.toString());
	}
    }
    
    // Activates the flag more Alerts pending indicatign that a new alert has been created/updated
    // or a former alerts has been switched on
    public static void activePendingAlerts(){
	    Tools.moreAlertsPending = true;
	    Tools.startPeriodicService(getContext());
    }

    public static void startPeriodicService(Context mContext){
	
		try{

		    mContext.startService(new Intent(mContext, PeriodicTimerService.class));

		}catch(Exception e){
		    Log.e(LOGGER_TAG,"Error startingService: " + e.toString());
		}
    }
    
    public static ArrayList<String> JSONArray2ArrayList(JSONArray jsonArray){
	ArrayList<String> list = new ArrayList<String>();     

	//JSONArray jsonArray = (JSONArray)jsonObject; 

	//JSONArray jPlaceIds = new JSONArray(jString);
	
	
	for (int i=0; i<jsonArray.length(); i++){
	    try{
	
		JSONObject jPlace = jsonArray.getJSONObject(i);

		list.add(jPlace.optString("display_name").toString());
	}catch(Exception e){
	    Log.e(LOGGER_TAG,"Error JSONArray2ArrayList: " + e.toString());
	}
	}
	
	/**
	if (jsonArray != null) { 
	    int len = jsonArray.length();
	    for (int i=0;i<len;i++){ 
		try{
		    list.add(jsonArray.get(i).toString());
		}catch(Exception e){
		    Log.e(LOGGER_TAG,"Error JSONArray2ArrayList: " + e.toString());
		}
	    } 
	}else{
	    list.add("");
	}
	*/

	return list;
    }
    
    
    public static GeoPoint getGeoPointOfAddress(JSONArray jsonArray, String address){

	GeoPoint point;
	Double lat = Double.valueOf(0.0);
	Double lon = Double.valueOf(0.0);

	try{

	    for (int i=0; i<jsonArray.length(); i++){

		JSONObject jPlace = jsonArray.getJSONObject(i);

		if (jPlace.optString("display_name").toString().compareTo(address)==0){

		    lat = jPlace.getDouble("lat");
		    lon = jPlace.getDouble("lon");
		}

	    }

	}catch(Exception e){
	    Log.e(LOGGER_TAG,"Error JSONArray2ArrayList: " + e.toString());
	}

	point = new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));

	return point;
    }

    /**
    public static void getGMTTime() {
	Calendar current = Calendar.getInstance();
	// txtCurrentTime.setText("" + current.getTime());

	long miliSeconds = current.getTimeInMillis();

	TimeZone tzCurrent = current.getTimeZone();

	int offset = tzCurrent.getRawOffset();

	if (tzCurrent.inDaylightTime(new Date())) {
	    offset = offset + tzCurrent.getDSTSavings();
	}

	miliSeconds = miliSeconds - offset;

	Date resultdate = new Date(miliSeconds);
	//System.out.println(sdf.format(resultdate));
    }
     */
}
