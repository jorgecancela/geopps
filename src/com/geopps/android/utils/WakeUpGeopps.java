package com.geopps.android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.geopps.android.AlertsActivity;

/**
 * 
 * When the Android device starts it broadcasts this message android.intent.action.BOOT_COMPLETED
 * to let the rest of the systems knows that OS is ready.
 * 
 * This class is waiting for this signal and when it arrives it starts the PeriodicTimerService
 * 
 * Geopps Java source code
 * @author jcancela (jcancelaglez@gmail.com)
 * @copyright: jcancela
 * @license: GPL3
*/
public class WakeUpGeopps extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
//	Toast.makeText(context, "todo bieeeeeeeennnnnnnennnnne", 1000).show();

	try {
	    //Intent k = new Intent(context, AlertsActivity.class);
	    //k.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    
	    //context.startActivity(k);

	    Log.v("StartService","Broadcast receiver:  Go In ");

		try{

		    context.startService(new Intent(context, PeriodicTimerService.class));

		}catch(Exception e){
		    Log.e("StartService","Error startingService: " + e.toString());
		}
    
		    Log.v("StartService","Service started?");

	    
	} catch (Exception e) {
	    Log.e("PeriodicTimerService", "Could not start service " + e.toString());
	}
    }
}